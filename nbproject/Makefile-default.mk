#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=sources/pic18/plib/ADC/adcbusy.c sources/pic18/plib/ADC/adcclose.c sources/pic18/plib/ADC/adcconv.c sources/pic18/plib/ADC/adcopen.c sources/pic18/plib/ADC/adcread.c sources/pic18/plib/ADC/adcselchconv.c sources/pic18/plib/ADC/adcsetch.c sources/pic18/plib/ancom/close_ancomp.c sources/pic18/plib/ancom/open_ancomp.c sources/pic18/plib/CTMU/CloseCTMU.c sources/pic18/plib/CTMU/CurrentControlCTMU.c sources/pic18/plib/CTMU/OpenCTMU.c sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.c sources/pic18/plib/DPSLP/GotoDeepSleep.c sources/pic18/plib/DPSLP/IsResetFromDeepSleep.c sources/pic18/plib/DPSLP/ReadDSGPR.c sources/pic18/plib/DPSLP/ULPWakeUpEnable.c sources/pic18/plib/EEP/busy_eep.c sources/pic18/plib/EEP/read_B.c sources/pic18/plib/EEP/write_B.c sources/pic18/plib/Flash/EraseFlash.c sources/pic18/plib/Flash/ReadFlash.c sources/pic18/plib/Flash/WriteBlockFlash.c sources/pic18/plib/Flash/WriteBytesFlash.c sources/pic18/plib/Flash/WriteWordFlash.c sources/pic18/plib/i2c/i2c1ack.c sources/pic18/plib/i2c/i2c1clos.c sources/pic18/plib/i2c/i2c1dtrd.c sources/pic18/plib/i2c/i2c1eeap.c sources/pic18/plib/i2c/i2c1eebw.c sources/pic18/plib/i2c/i2c1eecr.c sources/pic18/plib/i2c/i2c1eepw.c sources/pic18/plib/i2c/i2c1eerr.c sources/pic18/plib/i2c/i2c1eesr.c sources/pic18/plib/i2c/i2c1gets.c sources/pic18/plib/i2c/i2c1idle.c sources/pic18/plib/i2c/i2c1nack.c sources/pic18/plib/i2c/i2c1open.c sources/pic18/plib/i2c/i2c1puts.c sources/pic18/plib/i2c/i2c1read.c sources/pic18/plib/i2c/i2c1rstr.c sources/pic18/plib/i2c/i2c1stop.c sources/pic18/plib/i2c/i2c1strt.c sources/pic18/plib/i2c/i2c1writ.c sources/pic18/plib/i2c/i2c2ack.c sources/pic18/plib/i2c/i2c2clos.c sources/pic18/plib/i2c/i2c2dtrd.c sources/pic18/plib/i2c/i2c2eeap.c sources/pic18/plib/i2c/i2c2eebw.c sources/pic18/plib/i2c/i2c2eecr.c sources/pic18/plib/i2c/i2c2eepw.c sources/pic18/plib/i2c/i2c2eerr.c sources/pic18/plib/i2c/i2c2eesr.c sources/pic18/plib/i2c/i2c2gets.c sources/pic18/plib/i2c/i2c2idle.c sources/pic18/plib/i2c/i2c2nack.c sources/pic18/plib/i2c/i2c2open.c sources/pic18/plib/i2c/i2c2puts.c sources/pic18/plib/i2c/i2c2read.c sources/pic18/plib/i2c/i2c2rstr.c sources/pic18/plib/i2c/i2c2stop.c sources/pic18/plib/i2c/i2c2strt.c sources/pic18/plib/i2c/i2c2writ.c sources/pic18/plib/i2c/i2c_ack.c sources/pic18/plib/i2c/i2c_clos.c sources/pic18/plib/i2c/i2c_dtrd.c sources/pic18/plib/i2c/i2c_eeap.c sources/pic18/plib/i2c/i2c_eebw.c sources/pic18/plib/i2c/i2c_eecr.c sources/pic18/plib/i2c/i2c_eepw.c sources/pic18/plib/i2c/i2c_eerr.c sources/pic18/plib/i2c/i2c_eesr.c sources/pic18/plib/i2c/i2c_gets.c sources/pic18/plib/i2c/i2c_idle.c sources/pic18/plib/i2c/i2c_nack.c sources/pic18/plib/i2c/i2c_open.c sources/pic18/plib/i2c/i2c_puts.c sources/pic18/plib/i2c/i2c_read.c sources/pic18/plib/i2c/i2c_rstr.c sources/pic18/plib/i2c/i2c_stop.c sources/pic18/plib/i2c/i2c_strt.c sources/pic18/plib/i2c/i2c_writ.c sources/pic18/plib/mwire/mw1close.c sources/pic18/plib/mwire/mw1drdy.c sources/pic18/plib/mwire/mw1gets.c sources/pic18/plib/mwire/mw1open.c sources/pic18/plib/mwire/mw1read.c sources/pic18/plib/mwire/mw1write.c sources/pic18/plib/mwire/mw2close.c sources/pic18/plib/mwire/mw2drdy.c sources/pic18/plib/mwire/mw2gets.c sources/pic18/plib/mwire/mw2open.c sources/pic18/plib/mwire/mw2read.c sources/pic18/plib/mwire/mw2write.c sources/pic18/plib/mwire/mw_close.c sources/pic18/plib/mwire/mw_drdy.c sources/pic18/plib/mwire/mw_gets.c sources/pic18/plib/mwire/mw_open.c sources/pic18/plib/mwire/mw_read.c sources/pic18/plib/mwire/mw_write.c sources/pic18/plib/pcpwm/close_pcpwm.c sources/pic18/plib/pcpwm/open_pcpwm.c sources/pic18/plib/pcpwm/pcpwm_dt.c sources/pic18/plib/pcpwm/pcpwm_OVD.c sources/pic18/plib/pcpwm/set_dcpcpwm.c sources/pic18/plib/PMP/PMPClose.c sources/pic18/plib/PMP/PMPIsBUfferNEmpty.c sources/pic18/plib/PMP/PMPIsBufferNFull.c sources/pic18/plib/PMP/PMPMasterRead.c sources/pic18/plib/PMP/PMPMasterWrite.c sources/pic18/plib/PMP/PMPOpen.c sources/pic18/plib/PMP/PMPSetAddress.c sources/pic18/plib/PMP/PMPSlaveReadBufferN.c sources/pic18/plib/PMP/PMPSlaveReadBuffers.c sources/pic18/plib/PMP/PMPSlaveWriteBufferN.c sources/pic18/plib/PMP/PMPSlaveWriteBuffers.c sources/pic18/plib/PORTB/pbclose.c sources/pic18/plib/PORTB/pbopen.c sources/pic18/plib/PORTB/pulldis.c sources/pic18/plib/PORTB/pullen.c sources/pic18/plib/PORTB/rb0close.c sources/pic18/plib/PORTB/rb0open.c sources/pic18/plib/PORTB/rb1close.c sources/pic18/plib/PORTB/rb1open.c sources/pic18/plib/PORTB/rb2close.c sources/pic18/plib/PORTB/rb2open.c sources/pic18/plib/PORTB/rb3close.c sources/pic18/plib/PORTB/rb3open.c sources/pic18/plib/rtcc/RTCCInitClock.c sources/pic18/plib/rtcc/RtccReadAlrmDate.c sources/pic18/plib/rtcc/RtccReadAlrmTime.c sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.c sources/pic18/plib/rtcc/RtccReadDate.c sources/pic18/plib/rtcc/RtccReadTime.c sources/pic18/plib/rtcc/RtccReadTimeDate.c sources/pic18/plib/rtcc/RtccSetAlarmRpt.c sources/pic18/plib/rtcc/RtccSetAlarmRptCount.c sources/pic18/plib/rtcc/RtccSetCalibration.c sources/pic18/plib/rtcc/RtccSetChimeEnbl.c sources/pic18/plib/rtcc/RtccWriteAlrmDate.c sources/pic18/plib/rtcc/RtccWriteAlrmTime.c sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.c sources/pic18/plib/rtcc/RtccWriteDate.c sources/pic18/plib/rtcc/RtccWriteTime.c sources/pic18/plib/rtcc/RtccWriteTimeDate.c sources/pic18/plib/rtcc/RtccWrOn.c sources/pic18/plib/SPI/spi1clos.c sources/pic18/plib/SPI/spi1dtrd.c sources/pic18/plib/SPI/spi1gets.c sources/pic18/plib/SPI/spi1open.c sources/pic18/plib/SPI/spi1puts.c sources/pic18/plib/SPI/spi1read.c sources/pic18/plib/SPI/spi1writ.c sources/pic18/plib/SPI/spi2clos.c sources/pic18/plib/SPI/spi2dtrd.c sources/pic18/plib/SPI/spi2gets.c sources/pic18/plib/SPI/spi2open.c sources/pic18/plib/SPI/spi2puts.c sources/pic18/plib/SPI/spi2read.c sources/pic18/plib/SPI/spi2writ.c sources/pic18/plib/SPI/spi_clos.c sources/pic18/plib/SPI/spi_dtrd.c sources/pic18/plib/SPI/spi_gets.c sources/pic18/plib/SPI/spi_open.c sources/pic18/plib/SPI/spi_puts.c sources/pic18/plib/SPI/spi_read.c sources/pic18/plib/SPI/spi_writ.c sources/pic18/plib/SW_I2C/swacki2c.c sources/pic18/plib/SW_I2C/swckti2c.c sources/pic18/plib/SW_I2C/swgtci2c.c sources/pic18/plib/SW_I2C/swgtsi2c.c sources/pic18/plib/SW_I2C/swptci2c.c sources/pic18/plib/SW_I2C/swptsi2c.c sources/pic18/plib/SW_I2C/swrsti2c.c sources/pic18/plib/SW_I2C/swstpi2c.c sources/pic18/plib/SW_I2C/swstri2c.c sources/pic18/plib/SW_RTCC/close_rtcc.c sources/pic18/plib/SW_RTCC/open_rtcc.c sources/pic18/plib/SW_RTCC/update_rtcc.c sources/pic18/plib/SW_SPI/clrcsspi.c sources/pic18/plib/SW_SPI/opensspi.c sources/pic18/plib/SW_SPI/setcsspi.c sources/pic18/plib/SW_SPI/wrtsspi.c sources/pic18/plib/SW_UART/openuart.c sources/pic18/plib/SW_UART/sw_uart.c sources/pic18/plib/SW_UART/uartdata.c sources/pic18/plib/Timers/t0close.c sources/pic18/plib/Timers/t0open.c sources/pic18/plib/Timers/t0read.c sources/pic18/plib/Timers/t0write.c sources/pic18/plib/Timers/t10close.c sources/pic18/plib/Timers/t10open.c sources/pic18/plib/Timers/t10read.c sources/pic18/plib/Timers/t10write.c sources/pic18/plib/Timers/t12close.c sources/pic18/plib/Timers/t12open.c sources/pic18/plib/Timers/t12read.c sources/pic18/plib/Timers/t12write.c sources/pic18/plib/Timers/t1close.c sources/pic18/plib/Timers/t1open.c sources/pic18/plib/Timers/t1read.c sources/pic18/plib/Timers/t1write.c sources/pic18/plib/Timers/t2close.c sources/pic18/plib/Timers/t2open.c sources/pic18/plib/Timers/t2read.c sources/pic18/plib/Timers/t2write.c sources/pic18/plib/Timers/t3close.c sources/pic18/plib/Timers/t3open.c sources/pic18/plib/Timers/t3read.c sources/pic18/plib/Timers/t3write.c sources/pic18/plib/Timers/t4close.c sources/pic18/plib/Timers/t4open.c sources/pic18/plib/Timers/t4read.c sources/pic18/plib/Timers/t4write.c sources/pic18/plib/Timers/t5close.c sources/pic18/plib/Timers/t5open.c sources/pic18/plib/Timers/t5read.c sources/pic18/plib/Timers/t5write.c sources/pic18/plib/Timers/t6close.c sources/pic18/plib/Timers/t6open.c sources/pic18/plib/Timers/t6read.c sources/pic18/plib/Timers/t6write.c sources/pic18/plib/Timers/t7close.c sources/pic18/plib/Timers/t7open.c sources/pic18/plib/Timers/t7read.c sources/pic18/plib/Timers/t7write.c sources/pic18/plib/Timers/t8close.c sources/pic18/plib/Timers/t8open.c sources/pic18/plib/Timers/t8read.c sources/pic18/plib/Timers/t8write.c sources/pic18/plib/Timers/tmrccpsrc.c sources/pic18/plib/USART/u1baud.c sources/pic18/plib/USART/u1busy.c sources/pic18/plib/USART/u1close.c sources/pic18/plib/USART/u1defs.c sources/pic18/plib/USART/u1drdy.c sources/pic18/plib/USART/u1gets.c sources/pic18/plib/USART/u1open.c sources/pic18/plib/USART/u1putrs.c sources/pic18/plib/USART/u1puts.c sources/pic18/plib/USART/u1read.c sources/pic18/plib/USART/u1write.c sources/pic18/plib/USART/u2baud.c sources/pic18/plib/USART/u2busy.c sources/pic18/plib/USART/u2close.c sources/pic18/plib/USART/u2defs.c sources/pic18/plib/USART/u2drdy.c sources/pic18/plib/USART/u2gets.c sources/pic18/plib/USART/u2open.c sources/pic18/plib/USART/u2putrs.c sources/pic18/plib/USART/u2puts.c sources/pic18/plib/USART/u2read.c sources/pic18/plib/USART/u2write.c sources/pic18/plib/USART/u3baud.c sources/pic18/plib/USART/u3busy.c sources/pic18/plib/USART/u3close.c sources/pic18/plib/USART/u3defs.c sources/pic18/plib/USART/u3drdy.c sources/pic18/plib/USART/u3gets.c sources/pic18/plib/USART/u3open.c sources/pic18/plib/USART/u3putrs.c sources/pic18/plib/USART/u3puts.c sources/pic18/plib/USART/u3read.c sources/pic18/plib/USART/u3write.c sources/pic18/plib/USART/u4baud.c sources/pic18/plib/USART/u4busy.c sources/pic18/plib/USART/u4close.c sources/pic18/plib/USART/u4defs.c sources/pic18/plib/USART/u4drdy.c sources/pic18/plib/USART/u4gets.c sources/pic18/plib/USART/u4open.c sources/pic18/plib/USART/u4putrs.c sources/pic18/plib/USART/u4puts.c sources/pic18/plib/USART/u4read.c sources/pic18/plib/USART/u4write.c sources/pic18/plib/USART/ubaud.c sources/pic18/plib/USART/ubusy.c sources/pic18/plib/USART/uclose.c sources/pic18/plib/USART/udefs.c sources/pic18/plib/USART/udrdy.c sources/pic18/plib/USART/ugets.c sources/pic18/plib/USART/uopen.c sources/pic18/plib/USART/uputrs.c sources/pic18/plib/USART/uputs.c sources/pic18/plib/USART/uread.c sources/pic18/plib/USART/uwrite.c sources/pic18/plib/XLCD/busyxlcd.c sources/pic18/plib/XLCD/openxlcd.c sources/pic18/plib/XLCD/putrxlcd.c sources/pic18/plib/XLCD/putsxlcd.c sources/pic18/plib/XLCD/readaddr.c sources/pic18/plib/XLCD/readdata.c sources/pic18/plib/XLCD/setcgram.c sources/pic18/plib/XLCD/setddram.c sources/pic18/plib/XLCD/wcmdxlcd.c sources/pic18/plib/XLCD/writdata.c main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1 ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1 ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1 ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1 ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1 ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1 ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1 ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1 ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1 ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1 ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1 ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1 ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1 ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1 ${OBJECTDIR}/main.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1.d ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1.d ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1.d ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1.d ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1.d ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1.d ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1.d ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1.d ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1.d ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1.d ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1.d ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1.d ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1.d ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1.d ${OBJECTDIR}/main.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1 ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1 ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1 ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1 ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1 ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1 ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1 ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1 ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1 ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1 ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1 ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1 ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1 ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1 ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1 ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1 ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1 ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1 ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1 ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1 ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1 ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1 ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1 ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1 ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1 ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1 ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1 ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1 ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1 ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1 ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1 ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1 ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1 ${OBJECTDIR}/main.p1

# Source Files
SOURCEFILES=sources/pic18/plib/ADC/adcbusy.c sources/pic18/plib/ADC/adcclose.c sources/pic18/plib/ADC/adcconv.c sources/pic18/plib/ADC/adcopen.c sources/pic18/plib/ADC/adcread.c sources/pic18/plib/ADC/adcselchconv.c sources/pic18/plib/ADC/adcsetch.c sources/pic18/plib/ancom/close_ancomp.c sources/pic18/plib/ancom/open_ancomp.c sources/pic18/plib/CTMU/CloseCTMU.c sources/pic18/plib/CTMU/CurrentControlCTMU.c sources/pic18/plib/CTMU/OpenCTMU.c sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.c sources/pic18/plib/DPSLP/GotoDeepSleep.c sources/pic18/plib/DPSLP/IsResetFromDeepSleep.c sources/pic18/plib/DPSLP/ReadDSGPR.c sources/pic18/plib/DPSLP/ULPWakeUpEnable.c sources/pic18/plib/EEP/busy_eep.c sources/pic18/plib/EEP/read_B.c sources/pic18/plib/EEP/write_B.c sources/pic18/plib/Flash/EraseFlash.c sources/pic18/plib/Flash/ReadFlash.c sources/pic18/plib/Flash/WriteBlockFlash.c sources/pic18/plib/Flash/WriteBytesFlash.c sources/pic18/plib/Flash/WriteWordFlash.c sources/pic18/plib/i2c/i2c1ack.c sources/pic18/plib/i2c/i2c1clos.c sources/pic18/plib/i2c/i2c1dtrd.c sources/pic18/plib/i2c/i2c1eeap.c sources/pic18/plib/i2c/i2c1eebw.c sources/pic18/plib/i2c/i2c1eecr.c sources/pic18/plib/i2c/i2c1eepw.c sources/pic18/plib/i2c/i2c1eerr.c sources/pic18/plib/i2c/i2c1eesr.c sources/pic18/plib/i2c/i2c1gets.c sources/pic18/plib/i2c/i2c1idle.c sources/pic18/plib/i2c/i2c1nack.c sources/pic18/plib/i2c/i2c1open.c sources/pic18/plib/i2c/i2c1puts.c sources/pic18/plib/i2c/i2c1read.c sources/pic18/plib/i2c/i2c1rstr.c sources/pic18/plib/i2c/i2c1stop.c sources/pic18/plib/i2c/i2c1strt.c sources/pic18/plib/i2c/i2c1writ.c sources/pic18/plib/i2c/i2c2ack.c sources/pic18/plib/i2c/i2c2clos.c sources/pic18/plib/i2c/i2c2dtrd.c sources/pic18/plib/i2c/i2c2eeap.c sources/pic18/plib/i2c/i2c2eebw.c sources/pic18/plib/i2c/i2c2eecr.c sources/pic18/plib/i2c/i2c2eepw.c sources/pic18/plib/i2c/i2c2eerr.c sources/pic18/plib/i2c/i2c2eesr.c sources/pic18/plib/i2c/i2c2gets.c sources/pic18/plib/i2c/i2c2idle.c sources/pic18/plib/i2c/i2c2nack.c sources/pic18/plib/i2c/i2c2open.c sources/pic18/plib/i2c/i2c2puts.c sources/pic18/plib/i2c/i2c2read.c sources/pic18/plib/i2c/i2c2rstr.c sources/pic18/plib/i2c/i2c2stop.c sources/pic18/plib/i2c/i2c2strt.c sources/pic18/plib/i2c/i2c2writ.c sources/pic18/plib/i2c/i2c_ack.c sources/pic18/plib/i2c/i2c_clos.c sources/pic18/plib/i2c/i2c_dtrd.c sources/pic18/plib/i2c/i2c_eeap.c sources/pic18/plib/i2c/i2c_eebw.c sources/pic18/plib/i2c/i2c_eecr.c sources/pic18/plib/i2c/i2c_eepw.c sources/pic18/plib/i2c/i2c_eerr.c sources/pic18/plib/i2c/i2c_eesr.c sources/pic18/plib/i2c/i2c_gets.c sources/pic18/plib/i2c/i2c_idle.c sources/pic18/plib/i2c/i2c_nack.c sources/pic18/plib/i2c/i2c_open.c sources/pic18/plib/i2c/i2c_puts.c sources/pic18/plib/i2c/i2c_read.c sources/pic18/plib/i2c/i2c_rstr.c sources/pic18/plib/i2c/i2c_stop.c sources/pic18/plib/i2c/i2c_strt.c sources/pic18/plib/i2c/i2c_writ.c sources/pic18/plib/mwire/mw1close.c sources/pic18/plib/mwire/mw1drdy.c sources/pic18/plib/mwire/mw1gets.c sources/pic18/plib/mwire/mw1open.c sources/pic18/plib/mwire/mw1read.c sources/pic18/plib/mwire/mw1write.c sources/pic18/plib/mwire/mw2close.c sources/pic18/plib/mwire/mw2drdy.c sources/pic18/plib/mwire/mw2gets.c sources/pic18/plib/mwire/mw2open.c sources/pic18/plib/mwire/mw2read.c sources/pic18/plib/mwire/mw2write.c sources/pic18/plib/mwire/mw_close.c sources/pic18/plib/mwire/mw_drdy.c sources/pic18/plib/mwire/mw_gets.c sources/pic18/plib/mwire/mw_open.c sources/pic18/plib/mwire/mw_read.c sources/pic18/plib/mwire/mw_write.c sources/pic18/plib/pcpwm/close_pcpwm.c sources/pic18/plib/pcpwm/open_pcpwm.c sources/pic18/plib/pcpwm/pcpwm_dt.c sources/pic18/plib/pcpwm/pcpwm_OVD.c sources/pic18/plib/pcpwm/set_dcpcpwm.c sources/pic18/plib/PMP/PMPClose.c sources/pic18/plib/PMP/PMPIsBUfferNEmpty.c sources/pic18/plib/PMP/PMPIsBufferNFull.c sources/pic18/plib/PMP/PMPMasterRead.c sources/pic18/plib/PMP/PMPMasterWrite.c sources/pic18/plib/PMP/PMPOpen.c sources/pic18/plib/PMP/PMPSetAddress.c sources/pic18/plib/PMP/PMPSlaveReadBufferN.c sources/pic18/plib/PMP/PMPSlaveReadBuffers.c sources/pic18/plib/PMP/PMPSlaveWriteBufferN.c sources/pic18/plib/PMP/PMPSlaveWriteBuffers.c sources/pic18/plib/PORTB/pbclose.c sources/pic18/plib/PORTB/pbopen.c sources/pic18/plib/PORTB/pulldis.c sources/pic18/plib/PORTB/pullen.c sources/pic18/plib/PORTB/rb0close.c sources/pic18/plib/PORTB/rb0open.c sources/pic18/plib/PORTB/rb1close.c sources/pic18/plib/PORTB/rb1open.c sources/pic18/plib/PORTB/rb2close.c sources/pic18/plib/PORTB/rb2open.c sources/pic18/plib/PORTB/rb3close.c sources/pic18/plib/PORTB/rb3open.c sources/pic18/plib/rtcc/RTCCInitClock.c sources/pic18/plib/rtcc/RtccReadAlrmDate.c sources/pic18/plib/rtcc/RtccReadAlrmTime.c sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.c sources/pic18/plib/rtcc/RtccReadDate.c sources/pic18/plib/rtcc/RtccReadTime.c sources/pic18/plib/rtcc/RtccReadTimeDate.c sources/pic18/plib/rtcc/RtccSetAlarmRpt.c sources/pic18/plib/rtcc/RtccSetAlarmRptCount.c sources/pic18/plib/rtcc/RtccSetCalibration.c sources/pic18/plib/rtcc/RtccSetChimeEnbl.c sources/pic18/plib/rtcc/RtccWriteAlrmDate.c sources/pic18/plib/rtcc/RtccWriteAlrmTime.c sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.c sources/pic18/plib/rtcc/RtccWriteDate.c sources/pic18/plib/rtcc/RtccWriteTime.c sources/pic18/plib/rtcc/RtccWriteTimeDate.c sources/pic18/plib/rtcc/RtccWrOn.c sources/pic18/plib/SPI/spi1clos.c sources/pic18/plib/SPI/spi1dtrd.c sources/pic18/plib/SPI/spi1gets.c sources/pic18/plib/SPI/spi1open.c sources/pic18/plib/SPI/spi1puts.c sources/pic18/plib/SPI/spi1read.c sources/pic18/plib/SPI/spi1writ.c sources/pic18/plib/SPI/spi2clos.c sources/pic18/plib/SPI/spi2dtrd.c sources/pic18/plib/SPI/spi2gets.c sources/pic18/plib/SPI/spi2open.c sources/pic18/plib/SPI/spi2puts.c sources/pic18/plib/SPI/spi2read.c sources/pic18/plib/SPI/spi2writ.c sources/pic18/plib/SPI/spi_clos.c sources/pic18/plib/SPI/spi_dtrd.c sources/pic18/plib/SPI/spi_gets.c sources/pic18/plib/SPI/spi_open.c sources/pic18/plib/SPI/spi_puts.c sources/pic18/plib/SPI/spi_read.c sources/pic18/plib/SPI/spi_writ.c sources/pic18/plib/SW_I2C/swacki2c.c sources/pic18/plib/SW_I2C/swckti2c.c sources/pic18/plib/SW_I2C/swgtci2c.c sources/pic18/plib/SW_I2C/swgtsi2c.c sources/pic18/plib/SW_I2C/swptci2c.c sources/pic18/plib/SW_I2C/swptsi2c.c sources/pic18/plib/SW_I2C/swrsti2c.c sources/pic18/plib/SW_I2C/swstpi2c.c sources/pic18/plib/SW_I2C/swstri2c.c sources/pic18/plib/SW_RTCC/close_rtcc.c sources/pic18/plib/SW_RTCC/open_rtcc.c sources/pic18/plib/SW_RTCC/update_rtcc.c sources/pic18/plib/SW_SPI/clrcsspi.c sources/pic18/plib/SW_SPI/opensspi.c sources/pic18/plib/SW_SPI/setcsspi.c sources/pic18/plib/SW_SPI/wrtsspi.c sources/pic18/plib/SW_UART/openuart.c sources/pic18/plib/SW_UART/sw_uart.c sources/pic18/plib/SW_UART/uartdata.c sources/pic18/plib/Timers/t0close.c sources/pic18/plib/Timers/t0open.c sources/pic18/plib/Timers/t0read.c sources/pic18/plib/Timers/t0write.c sources/pic18/plib/Timers/t10close.c sources/pic18/plib/Timers/t10open.c sources/pic18/plib/Timers/t10read.c sources/pic18/plib/Timers/t10write.c sources/pic18/plib/Timers/t12close.c sources/pic18/plib/Timers/t12open.c sources/pic18/plib/Timers/t12read.c sources/pic18/plib/Timers/t12write.c sources/pic18/plib/Timers/t1close.c sources/pic18/plib/Timers/t1open.c sources/pic18/plib/Timers/t1read.c sources/pic18/plib/Timers/t1write.c sources/pic18/plib/Timers/t2close.c sources/pic18/plib/Timers/t2open.c sources/pic18/plib/Timers/t2read.c sources/pic18/plib/Timers/t2write.c sources/pic18/plib/Timers/t3close.c sources/pic18/plib/Timers/t3open.c sources/pic18/plib/Timers/t3read.c sources/pic18/plib/Timers/t3write.c sources/pic18/plib/Timers/t4close.c sources/pic18/plib/Timers/t4open.c sources/pic18/plib/Timers/t4read.c sources/pic18/plib/Timers/t4write.c sources/pic18/plib/Timers/t5close.c sources/pic18/plib/Timers/t5open.c sources/pic18/plib/Timers/t5read.c sources/pic18/plib/Timers/t5write.c sources/pic18/plib/Timers/t6close.c sources/pic18/plib/Timers/t6open.c sources/pic18/plib/Timers/t6read.c sources/pic18/plib/Timers/t6write.c sources/pic18/plib/Timers/t7close.c sources/pic18/plib/Timers/t7open.c sources/pic18/plib/Timers/t7read.c sources/pic18/plib/Timers/t7write.c sources/pic18/plib/Timers/t8close.c sources/pic18/plib/Timers/t8open.c sources/pic18/plib/Timers/t8read.c sources/pic18/plib/Timers/t8write.c sources/pic18/plib/Timers/tmrccpsrc.c sources/pic18/plib/USART/u1baud.c sources/pic18/plib/USART/u1busy.c sources/pic18/plib/USART/u1close.c sources/pic18/plib/USART/u1defs.c sources/pic18/plib/USART/u1drdy.c sources/pic18/plib/USART/u1gets.c sources/pic18/plib/USART/u1open.c sources/pic18/plib/USART/u1putrs.c sources/pic18/plib/USART/u1puts.c sources/pic18/plib/USART/u1read.c sources/pic18/plib/USART/u1write.c sources/pic18/plib/USART/u2baud.c sources/pic18/plib/USART/u2busy.c sources/pic18/plib/USART/u2close.c sources/pic18/plib/USART/u2defs.c sources/pic18/plib/USART/u2drdy.c sources/pic18/plib/USART/u2gets.c sources/pic18/plib/USART/u2open.c sources/pic18/plib/USART/u2putrs.c sources/pic18/plib/USART/u2puts.c sources/pic18/plib/USART/u2read.c sources/pic18/plib/USART/u2write.c sources/pic18/plib/USART/u3baud.c sources/pic18/plib/USART/u3busy.c sources/pic18/plib/USART/u3close.c sources/pic18/plib/USART/u3defs.c sources/pic18/plib/USART/u3drdy.c sources/pic18/plib/USART/u3gets.c sources/pic18/plib/USART/u3open.c sources/pic18/plib/USART/u3putrs.c sources/pic18/plib/USART/u3puts.c sources/pic18/plib/USART/u3read.c sources/pic18/plib/USART/u3write.c sources/pic18/plib/USART/u4baud.c sources/pic18/plib/USART/u4busy.c sources/pic18/plib/USART/u4close.c sources/pic18/plib/USART/u4defs.c sources/pic18/plib/USART/u4drdy.c sources/pic18/plib/USART/u4gets.c sources/pic18/plib/USART/u4open.c sources/pic18/plib/USART/u4putrs.c sources/pic18/plib/USART/u4puts.c sources/pic18/plib/USART/u4read.c sources/pic18/plib/USART/u4write.c sources/pic18/plib/USART/ubaud.c sources/pic18/plib/USART/ubusy.c sources/pic18/plib/USART/uclose.c sources/pic18/plib/USART/udefs.c sources/pic18/plib/USART/udrdy.c sources/pic18/plib/USART/ugets.c sources/pic18/plib/USART/uopen.c sources/pic18/plib/USART/uputrs.c sources/pic18/plib/USART/uputs.c sources/pic18/plib/USART/uread.c sources/pic18/plib/USART/uwrite.c sources/pic18/plib/XLCD/busyxlcd.c sources/pic18/plib/XLCD/openxlcd.c sources/pic18/plib/XLCD/putrxlcd.c sources/pic18/plib/XLCD/putsxlcd.c sources/pic18/plib/XLCD/readaddr.c sources/pic18/plib/XLCD/readdata.c sources/pic18/plib/XLCD/setcgram.c sources/pic18/plib/XLCD/setddram.c sources/pic18/plib/XLCD/wcmdxlcd.c sources/pic18/plib/XLCD/writdata.c main.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk ${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F2550
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1: sources/pic18/plib/ADC/adcbusy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1 sources/pic18/plib/ADC/adcbusy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1: sources/pic18/plib/ADC/adcclose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1 sources/pic18/plib/ADC/adcclose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1: sources/pic18/plib/ADC/adcconv.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1 sources/pic18/plib/ADC/adcconv.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1: sources/pic18/plib/ADC/adcopen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1 sources/pic18/plib/ADC/adcopen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1: sources/pic18/plib/ADC/adcread.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1 sources/pic18/plib/ADC/adcread.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1: sources/pic18/plib/ADC/adcselchconv.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1 sources/pic18/plib/ADC/adcselchconv.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1: sources/pic18/plib/ADC/adcsetch.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1 sources/pic18/plib/ADC/adcsetch.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1: sources/pic18/plib/ancom/close_ancomp.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ancom" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1 sources/pic18/plib/ancom/close_ancomp.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.d ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1: sources/pic18/plib/ancom/open_ancomp.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ancom" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1 sources/pic18/plib/ancom/open_ancomp.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.d ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1: sources/pic18/plib/CTMU/CloseCTMU.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/CTMU" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1 sources/pic18/plib/CTMU/CloseCTMU.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.d ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1: sources/pic18/plib/CTMU/CurrentControlCTMU.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/CTMU" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1 sources/pic18/plib/CTMU/CurrentControlCTMU.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.d ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1: sources/pic18/plib/CTMU/OpenCTMU.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/CTMU" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1 sources/pic18/plib/CTMU/OpenCTMU.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.d ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1: sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1 sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1: sources/pic18/plib/DPSLP/GotoDeepSleep.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1 sources/pic18/plib/DPSLP/GotoDeepSleep.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1: sources/pic18/plib/DPSLP/IsResetFromDeepSleep.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1 sources/pic18/plib/DPSLP/IsResetFromDeepSleep.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1: sources/pic18/plib/DPSLP/ReadDSGPR.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1 sources/pic18/plib/DPSLP/ReadDSGPR.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1: sources/pic18/plib/DPSLP/ULPWakeUpEnable.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1 sources/pic18/plib/DPSLP/ULPWakeUpEnable.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1: sources/pic18/plib/EEP/busy_eep.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/EEP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1 sources/pic18/plib/EEP/busy_eep.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.d ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1: sources/pic18/plib/EEP/read_B.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/EEP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1 sources/pic18/plib/EEP/read_B.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.d ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1: sources/pic18/plib/EEP/write_B.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/EEP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1 sources/pic18/plib/EEP/write_B.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.d ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1: sources/pic18/plib/Flash/EraseFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1 sources/pic18/plib/Flash/EraseFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1: sources/pic18/plib/Flash/ReadFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1 sources/pic18/plib/Flash/ReadFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1: sources/pic18/plib/Flash/WriteBlockFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1 sources/pic18/plib/Flash/WriteBlockFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1: sources/pic18/plib/Flash/WriteBytesFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1 sources/pic18/plib/Flash/WriteBytesFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1: sources/pic18/plib/Flash/WriteWordFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1 sources/pic18/plib/Flash/WriteWordFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1: sources/pic18/plib/i2c/i2c1ack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1 sources/pic18/plib/i2c/i2c1ack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1: sources/pic18/plib/i2c/i2c1clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1 sources/pic18/plib/i2c/i2c1clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1: sources/pic18/plib/i2c/i2c1dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1 sources/pic18/plib/i2c/i2c1dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1: sources/pic18/plib/i2c/i2c1eeap.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1 sources/pic18/plib/i2c/i2c1eeap.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1: sources/pic18/plib/i2c/i2c1eebw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1 sources/pic18/plib/i2c/i2c1eebw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1: sources/pic18/plib/i2c/i2c1eecr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1 sources/pic18/plib/i2c/i2c1eecr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1: sources/pic18/plib/i2c/i2c1eepw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1 sources/pic18/plib/i2c/i2c1eepw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1: sources/pic18/plib/i2c/i2c1eerr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1 sources/pic18/plib/i2c/i2c1eerr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1: sources/pic18/plib/i2c/i2c1eesr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1 sources/pic18/plib/i2c/i2c1eesr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1: sources/pic18/plib/i2c/i2c1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1 sources/pic18/plib/i2c/i2c1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1: sources/pic18/plib/i2c/i2c1idle.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1 sources/pic18/plib/i2c/i2c1idle.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1: sources/pic18/plib/i2c/i2c1nack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1 sources/pic18/plib/i2c/i2c1nack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1: sources/pic18/plib/i2c/i2c1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1 sources/pic18/plib/i2c/i2c1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1: sources/pic18/plib/i2c/i2c1puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1 sources/pic18/plib/i2c/i2c1puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1: sources/pic18/plib/i2c/i2c1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1 sources/pic18/plib/i2c/i2c1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1: sources/pic18/plib/i2c/i2c1rstr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1 sources/pic18/plib/i2c/i2c1rstr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1: sources/pic18/plib/i2c/i2c1stop.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1 sources/pic18/plib/i2c/i2c1stop.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1: sources/pic18/plib/i2c/i2c1strt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1 sources/pic18/plib/i2c/i2c1strt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1: sources/pic18/plib/i2c/i2c1writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1 sources/pic18/plib/i2c/i2c1writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1: sources/pic18/plib/i2c/i2c2ack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1 sources/pic18/plib/i2c/i2c2ack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1: sources/pic18/plib/i2c/i2c2clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1 sources/pic18/plib/i2c/i2c2clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1: sources/pic18/plib/i2c/i2c2dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1 sources/pic18/plib/i2c/i2c2dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1: sources/pic18/plib/i2c/i2c2eeap.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1 sources/pic18/plib/i2c/i2c2eeap.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1: sources/pic18/plib/i2c/i2c2eebw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1 sources/pic18/plib/i2c/i2c2eebw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1: sources/pic18/plib/i2c/i2c2eecr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1 sources/pic18/plib/i2c/i2c2eecr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1: sources/pic18/plib/i2c/i2c2eepw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1 sources/pic18/plib/i2c/i2c2eepw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1: sources/pic18/plib/i2c/i2c2eerr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1 sources/pic18/plib/i2c/i2c2eerr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1: sources/pic18/plib/i2c/i2c2eesr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1 sources/pic18/plib/i2c/i2c2eesr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1: sources/pic18/plib/i2c/i2c2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1 sources/pic18/plib/i2c/i2c2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1: sources/pic18/plib/i2c/i2c2idle.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1 sources/pic18/plib/i2c/i2c2idle.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1: sources/pic18/plib/i2c/i2c2nack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1 sources/pic18/plib/i2c/i2c2nack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1: sources/pic18/plib/i2c/i2c2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1 sources/pic18/plib/i2c/i2c2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1: sources/pic18/plib/i2c/i2c2puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1 sources/pic18/plib/i2c/i2c2puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1: sources/pic18/plib/i2c/i2c2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1 sources/pic18/plib/i2c/i2c2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1: sources/pic18/plib/i2c/i2c2rstr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1 sources/pic18/plib/i2c/i2c2rstr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1: sources/pic18/plib/i2c/i2c2stop.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1 sources/pic18/plib/i2c/i2c2stop.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1: sources/pic18/plib/i2c/i2c2strt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1 sources/pic18/plib/i2c/i2c2strt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1: sources/pic18/plib/i2c/i2c2writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1 sources/pic18/plib/i2c/i2c2writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1: sources/pic18/plib/i2c/i2c_ack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1 sources/pic18/plib/i2c/i2c_ack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1: sources/pic18/plib/i2c/i2c_clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1 sources/pic18/plib/i2c/i2c_clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1: sources/pic18/plib/i2c/i2c_dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1 sources/pic18/plib/i2c/i2c_dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1: sources/pic18/plib/i2c/i2c_eeap.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1 sources/pic18/plib/i2c/i2c_eeap.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1: sources/pic18/plib/i2c/i2c_eebw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1 sources/pic18/plib/i2c/i2c_eebw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1: sources/pic18/plib/i2c/i2c_eecr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1 sources/pic18/plib/i2c/i2c_eecr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1: sources/pic18/plib/i2c/i2c_eepw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1 sources/pic18/plib/i2c/i2c_eepw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1: sources/pic18/plib/i2c/i2c_eerr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1 sources/pic18/plib/i2c/i2c_eerr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1: sources/pic18/plib/i2c/i2c_eesr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1 sources/pic18/plib/i2c/i2c_eesr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1: sources/pic18/plib/i2c/i2c_gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1 sources/pic18/plib/i2c/i2c_gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1: sources/pic18/plib/i2c/i2c_idle.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1 sources/pic18/plib/i2c/i2c_idle.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1: sources/pic18/plib/i2c/i2c_nack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1 sources/pic18/plib/i2c/i2c_nack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1: sources/pic18/plib/i2c/i2c_open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1 sources/pic18/plib/i2c/i2c_open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1: sources/pic18/plib/i2c/i2c_puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1 sources/pic18/plib/i2c/i2c_puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1: sources/pic18/plib/i2c/i2c_read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1 sources/pic18/plib/i2c/i2c_read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1: sources/pic18/plib/i2c/i2c_rstr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1 sources/pic18/plib/i2c/i2c_rstr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1: sources/pic18/plib/i2c/i2c_stop.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1 sources/pic18/plib/i2c/i2c_stop.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1: sources/pic18/plib/i2c/i2c_strt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1 sources/pic18/plib/i2c/i2c_strt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1: sources/pic18/plib/i2c/i2c_writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1 sources/pic18/plib/i2c/i2c_writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1: sources/pic18/plib/mwire/mw1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1 sources/pic18/plib/mwire/mw1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1: sources/pic18/plib/mwire/mw1drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1 sources/pic18/plib/mwire/mw1drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1: sources/pic18/plib/mwire/mw1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1 sources/pic18/plib/mwire/mw1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1: sources/pic18/plib/mwire/mw1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1 sources/pic18/plib/mwire/mw1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1: sources/pic18/plib/mwire/mw1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1 sources/pic18/plib/mwire/mw1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1: sources/pic18/plib/mwire/mw1write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1 sources/pic18/plib/mwire/mw1write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1: sources/pic18/plib/mwire/mw2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1 sources/pic18/plib/mwire/mw2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1: sources/pic18/plib/mwire/mw2drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1 sources/pic18/plib/mwire/mw2drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1: sources/pic18/plib/mwire/mw2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1 sources/pic18/plib/mwire/mw2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1: sources/pic18/plib/mwire/mw2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1 sources/pic18/plib/mwire/mw2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1: sources/pic18/plib/mwire/mw2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1 sources/pic18/plib/mwire/mw2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1: sources/pic18/plib/mwire/mw2write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1 sources/pic18/plib/mwire/mw2write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1: sources/pic18/plib/mwire/mw_close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1 sources/pic18/plib/mwire/mw_close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1: sources/pic18/plib/mwire/mw_drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1 sources/pic18/plib/mwire/mw_drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1: sources/pic18/plib/mwire/mw_gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1 sources/pic18/plib/mwire/mw_gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1: sources/pic18/plib/mwire/mw_open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1 sources/pic18/plib/mwire/mw_open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1: sources/pic18/plib/mwire/mw_read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1 sources/pic18/plib/mwire/mw_read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1: sources/pic18/plib/mwire/mw_write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1 sources/pic18/plib/mwire/mw_write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1: sources/pic18/plib/pcpwm/close_pcpwm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1 sources/pic18/plib/pcpwm/close_pcpwm.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1: sources/pic18/plib/pcpwm/open_pcpwm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1 sources/pic18/plib/pcpwm/open_pcpwm.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1: sources/pic18/plib/pcpwm/pcpwm_dt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1 sources/pic18/plib/pcpwm/pcpwm_dt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1: sources/pic18/plib/pcpwm/pcpwm_OVD.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1 sources/pic18/plib/pcpwm/pcpwm_OVD.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1: sources/pic18/plib/pcpwm/set_dcpcpwm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1 sources/pic18/plib/pcpwm/set_dcpcpwm.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1: sources/pic18/plib/PMP/PMPClose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1 sources/pic18/plib/PMP/PMPClose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1: sources/pic18/plib/PMP/PMPIsBUfferNEmpty.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1 sources/pic18/plib/PMP/PMPIsBUfferNEmpty.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1: sources/pic18/plib/PMP/PMPIsBufferNFull.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1 sources/pic18/plib/PMP/PMPIsBufferNFull.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1: sources/pic18/plib/PMP/PMPMasterRead.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1 sources/pic18/plib/PMP/PMPMasterRead.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1: sources/pic18/plib/PMP/PMPMasterWrite.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1 sources/pic18/plib/PMP/PMPMasterWrite.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1: sources/pic18/plib/PMP/PMPOpen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1 sources/pic18/plib/PMP/PMPOpen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1: sources/pic18/plib/PMP/PMPSetAddress.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1 sources/pic18/plib/PMP/PMPSetAddress.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1: sources/pic18/plib/PMP/PMPSlaveReadBufferN.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1 sources/pic18/plib/PMP/PMPSlaveReadBufferN.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1: sources/pic18/plib/PMP/PMPSlaveReadBuffers.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1 sources/pic18/plib/PMP/PMPSlaveReadBuffers.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1: sources/pic18/plib/PMP/PMPSlaveWriteBufferN.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1 sources/pic18/plib/PMP/PMPSlaveWriteBufferN.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1: sources/pic18/plib/PMP/PMPSlaveWriteBuffers.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1 sources/pic18/plib/PMP/PMPSlaveWriteBuffers.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1: sources/pic18/plib/PORTB/pbclose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1 sources/pic18/plib/PORTB/pbclose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1: sources/pic18/plib/PORTB/pbopen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1 sources/pic18/plib/PORTB/pbopen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1: sources/pic18/plib/PORTB/pulldis.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1 sources/pic18/plib/PORTB/pulldis.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1: sources/pic18/plib/PORTB/pullen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1 sources/pic18/plib/PORTB/pullen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1: sources/pic18/plib/PORTB/rb0close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1 sources/pic18/plib/PORTB/rb0close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1: sources/pic18/plib/PORTB/rb0open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1 sources/pic18/plib/PORTB/rb0open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1: sources/pic18/plib/PORTB/rb1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1 sources/pic18/plib/PORTB/rb1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1: sources/pic18/plib/PORTB/rb1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1 sources/pic18/plib/PORTB/rb1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1: sources/pic18/plib/PORTB/rb2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1 sources/pic18/plib/PORTB/rb2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1: sources/pic18/plib/PORTB/rb2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1 sources/pic18/plib/PORTB/rb2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1: sources/pic18/plib/PORTB/rb3close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1 sources/pic18/plib/PORTB/rb3close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1: sources/pic18/plib/PORTB/rb3open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1 sources/pic18/plib/PORTB/rb3open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1: sources/pic18/plib/rtcc/RTCCInitClock.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1 sources/pic18/plib/rtcc/RTCCInitClock.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1: sources/pic18/plib/rtcc/RtccReadAlrmDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1 sources/pic18/plib/rtcc/RtccReadAlrmDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1: sources/pic18/plib/rtcc/RtccReadAlrmTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1 sources/pic18/plib/rtcc/RtccReadAlrmTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1: sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1 sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1: sources/pic18/plib/rtcc/RtccReadDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1 sources/pic18/plib/rtcc/RtccReadDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1: sources/pic18/plib/rtcc/RtccReadTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1 sources/pic18/plib/rtcc/RtccReadTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1: sources/pic18/plib/rtcc/RtccReadTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1 sources/pic18/plib/rtcc/RtccReadTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1: sources/pic18/plib/rtcc/RtccSetAlarmRpt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1 sources/pic18/plib/rtcc/RtccSetAlarmRpt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1: sources/pic18/plib/rtcc/RtccSetAlarmRptCount.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1 sources/pic18/plib/rtcc/RtccSetAlarmRptCount.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1: sources/pic18/plib/rtcc/RtccSetCalibration.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1 sources/pic18/plib/rtcc/RtccSetCalibration.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1: sources/pic18/plib/rtcc/RtccSetChimeEnbl.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1 sources/pic18/plib/rtcc/RtccSetChimeEnbl.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1: sources/pic18/plib/rtcc/RtccWriteAlrmDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1 sources/pic18/plib/rtcc/RtccWriteAlrmDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1: sources/pic18/plib/rtcc/RtccWriteAlrmTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1 sources/pic18/plib/rtcc/RtccWriteAlrmTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1: sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1 sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1: sources/pic18/plib/rtcc/RtccWriteDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1 sources/pic18/plib/rtcc/RtccWriteDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1: sources/pic18/plib/rtcc/RtccWriteTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1 sources/pic18/plib/rtcc/RtccWriteTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1: sources/pic18/plib/rtcc/RtccWriteTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1 sources/pic18/plib/rtcc/RtccWriteTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1: sources/pic18/plib/rtcc/RtccWrOn.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1 sources/pic18/plib/rtcc/RtccWrOn.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1: sources/pic18/plib/SPI/spi1clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1 sources/pic18/plib/SPI/spi1clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1: sources/pic18/plib/SPI/spi1dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1 sources/pic18/plib/SPI/spi1dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1: sources/pic18/plib/SPI/spi1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1 sources/pic18/plib/SPI/spi1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1: sources/pic18/plib/SPI/spi1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1 sources/pic18/plib/SPI/spi1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1: sources/pic18/plib/SPI/spi1puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1 sources/pic18/plib/SPI/spi1puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1: sources/pic18/plib/SPI/spi1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1 sources/pic18/plib/SPI/spi1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1: sources/pic18/plib/SPI/spi1writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1 sources/pic18/plib/SPI/spi1writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1: sources/pic18/plib/SPI/spi2clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1 sources/pic18/plib/SPI/spi2clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1: sources/pic18/plib/SPI/spi2dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1 sources/pic18/plib/SPI/spi2dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1: sources/pic18/plib/SPI/spi2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1 sources/pic18/plib/SPI/spi2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1: sources/pic18/plib/SPI/spi2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1 sources/pic18/plib/SPI/spi2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1: sources/pic18/plib/SPI/spi2puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1 sources/pic18/plib/SPI/spi2puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1: sources/pic18/plib/SPI/spi2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1 sources/pic18/plib/SPI/spi2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1: sources/pic18/plib/SPI/spi2writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1 sources/pic18/plib/SPI/spi2writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1: sources/pic18/plib/SPI/spi_clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1 sources/pic18/plib/SPI/spi_clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1: sources/pic18/plib/SPI/spi_dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1 sources/pic18/plib/SPI/spi_dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1: sources/pic18/plib/SPI/spi_gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1 sources/pic18/plib/SPI/spi_gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1: sources/pic18/plib/SPI/spi_open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1 sources/pic18/plib/SPI/spi_open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1: sources/pic18/plib/SPI/spi_puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1 sources/pic18/plib/SPI/spi_puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1: sources/pic18/plib/SPI/spi_read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1 sources/pic18/plib/SPI/spi_read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1: sources/pic18/plib/SPI/spi_writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1 sources/pic18/plib/SPI/spi_writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1: sources/pic18/plib/SW_I2C/swacki2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1 sources/pic18/plib/SW_I2C/swacki2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1: sources/pic18/plib/SW_I2C/swckti2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1 sources/pic18/plib/SW_I2C/swckti2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1: sources/pic18/plib/SW_I2C/swgtci2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1 sources/pic18/plib/SW_I2C/swgtci2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1: sources/pic18/plib/SW_I2C/swgtsi2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1 sources/pic18/plib/SW_I2C/swgtsi2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1: sources/pic18/plib/SW_I2C/swptci2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1 sources/pic18/plib/SW_I2C/swptci2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1: sources/pic18/plib/SW_I2C/swptsi2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1 sources/pic18/plib/SW_I2C/swptsi2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1: sources/pic18/plib/SW_I2C/swrsti2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1 sources/pic18/plib/SW_I2C/swrsti2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1: sources/pic18/plib/SW_I2C/swstpi2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1 sources/pic18/plib/SW_I2C/swstpi2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1: sources/pic18/plib/SW_I2C/swstri2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1 sources/pic18/plib/SW_I2C/swstri2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1: sources/pic18/plib/SW_RTCC/close_rtcc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_RTCC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1 sources/pic18/plib/SW_RTCC/close_rtcc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1: sources/pic18/plib/SW_RTCC/open_rtcc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_RTCC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1 sources/pic18/plib/SW_RTCC/open_rtcc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1: sources/pic18/plib/SW_RTCC/update_rtcc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_RTCC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1 sources/pic18/plib/SW_RTCC/update_rtcc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1: sources/pic18/plib/SW_SPI/clrcsspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1 sources/pic18/plib/SW_SPI/clrcsspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1: sources/pic18/plib/SW_SPI/opensspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1 sources/pic18/plib/SW_SPI/opensspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1: sources/pic18/plib/SW_SPI/setcsspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1 sources/pic18/plib/SW_SPI/setcsspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1: sources/pic18/plib/SW_SPI/wrtsspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1 sources/pic18/plib/SW_SPI/wrtsspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1: sources/pic18/plib/SW_UART/openuart.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_UART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1 sources/pic18/plib/SW_UART/openuart.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1: sources/pic18/plib/SW_UART/sw_uart.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_UART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1 sources/pic18/plib/SW_UART/sw_uart.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1: sources/pic18/plib/SW_UART/uartdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_UART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1 sources/pic18/plib/SW_UART/uartdata.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1: sources/pic18/plib/Timers/t0close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1 sources/pic18/plib/Timers/t0close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1: sources/pic18/plib/Timers/t0open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1 sources/pic18/plib/Timers/t0open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1: sources/pic18/plib/Timers/t0read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1 sources/pic18/plib/Timers/t0read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1: sources/pic18/plib/Timers/t0write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1 sources/pic18/plib/Timers/t0write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1: sources/pic18/plib/Timers/t10close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1 sources/pic18/plib/Timers/t10close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1: sources/pic18/plib/Timers/t10open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1 sources/pic18/plib/Timers/t10open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1: sources/pic18/plib/Timers/t10read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1 sources/pic18/plib/Timers/t10read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1: sources/pic18/plib/Timers/t10write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1 sources/pic18/plib/Timers/t10write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1: sources/pic18/plib/Timers/t12close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1 sources/pic18/plib/Timers/t12close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1: sources/pic18/plib/Timers/t12open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1 sources/pic18/plib/Timers/t12open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1: sources/pic18/plib/Timers/t12read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1 sources/pic18/plib/Timers/t12read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1: sources/pic18/plib/Timers/t12write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1 sources/pic18/plib/Timers/t12write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1: sources/pic18/plib/Timers/t1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1 sources/pic18/plib/Timers/t1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1: sources/pic18/plib/Timers/t1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1 sources/pic18/plib/Timers/t1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1: sources/pic18/plib/Timers/t1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1 sources/pic18/plib/Timers/t1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1: sources/pic18/plib/Timers/t1write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1 sources/pic18/plib/Timers/t1write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1: sources/pic18/plib/Timers/t2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1 sources/pic18/plib/Timers/t2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1: sources/pic18/plib/Timers/t2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1 sources/pic18/plib/Timers/t2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1: sources/pic18/plib/Timers/t2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1 sources/pic18/plib/Timers/t2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1: sources/pic18/plib/Timers/t2write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1 sources/pic18/plib/Timers/t2write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1: sources/pic18/plib/Timers/t3close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1 sources/pic18/plib/Timers/t3close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1: sources/pic18/plib/Timers/t3open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1 sources/pic18/plib/Timers/t3open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1: sources/pic18/plib/Timers/t3read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1 sources/pic18/plib/Timers/t3read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1: sources/pic18/plib/Timers/t3write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1 sources/pic18/plib/Timers/t3write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1: sources/pic18/plib/Timers/t4close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1 sources/pic18/plib/Timers/t4close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1: sources/pic18/plib/Timers/t4open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1 sources/pic18/plib/Timers/t4open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1: sources/pic18/plib/Timers/t4read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1 sources/pic18/plib/Timers/t4read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1: sources/pic18/plib/Timers/t4write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1 sources/pic18/plib/Timers/t4write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1: sources/pic18/plib/Timers/t5close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1 sources/pic18/plib/Timers/t5close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1: sources/pic18/plib/Timers/t5open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1 sources/pic18/plib/Timers/t5open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1: sources/pic18/plib/Timers/t5read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1 sources/pic18/plib/Timers/t5read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1: sources/pic18/plib/Timers/t5write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1 sources/pic18/plib/Timers/t5write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1: sources/pic18/plib/Timers/t6close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1 sources/pic18/plib/Timers/t6close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1: sources/pic18/plib/Timers/t6open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1 sources/pic18/plib/Timers/t6open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1: sources/pic18/plib/Timers/t6read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1 sources/pic18/plib/Timers/t6read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1: sources/pic18/plib/Timers/t6write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1 sources/pic18/plib/Timers/t6write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1: sources/pic18/plib/Timers/t7close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1 sources/pic18/plib/Timers/t7close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1: sources/pic18/plib/Timers/t7open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1 sources/pic18/plib/Timers/t7open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1: sources/pic18/plib/Timers/t7read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1 sources/pic18/plib/Timers/t7read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1: sources/pic18/plib/Timers/t7write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1 sources/pic18/plib/Timers/t7write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1: sources/pic18/plib/Timers/t8close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1 sources/pic18/plib/Timers/t8close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1: sources/pic18/plib/Timers/t8open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1 sources/pic18/plib/Timers/t8open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1: sources/pic18/plib/Timers/t8read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1 sources/pic18/plib/Timers/t8read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1: sources/pic18/plib/Timers/t8write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1 sources/pic18/plib/Timers/t8write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1: sources/pic18/plib/Timers/tmrccpsrc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1 sources/pic18/plib/Timers/tmrccpsrc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.d ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1: sources/pic18/plib/USART/u1baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1 sources/pic18/plib/USART/u1baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1: sources/pic18/plib/USART/u1busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1 sources/pic18/plib/USART/u1busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1: sources/pic18/plib/USART/u1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1 sources/pic18/plib/USART/u1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.d ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1: sources/pic18/plib/USART/u1defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1 sources/pic18/plib/USART/u1defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1: sources/pic18/plib/USART/u1drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1 sources/pic18/plib/USART/u1drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1: sources/pic18/plib/USART/u1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1 sources/pic18/plib/USART/u1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1: sources/pic18/plib/USART/u1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1 sources/pic18/plib/USART/u1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.d ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1: sources/pic18/plib/USART/u1putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1 sources/pic18/plib/USART/u1putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1: sources/pic18/plib/USART/u1puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1 sources/pic18/plib/USART/u1puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1: sources/pic18/plib/USART/u1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1 sources/pic18/plib/USART/u1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.d ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1: sources/pic18/plib/USART/u1write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1 sources/pic18/plib/USART/u1write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.d ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1: sources/pic18/plib/USART/u2baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1 sources/pic18/plib/USART/u2baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1: sources/pic18/plib/USART/u2busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1 sources/pic18/plib/USART/u2busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1: sources/pic18/plib/USART/u2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1 sources/pic18/plib/USART/u2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.d ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1: sources/pic18/plib/USART/u2defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1 sources/pic18/plib/USART/u2defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1: sources/pic18/plib/USART/u2drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1 sources/pic18/plib/USART/u2drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1: sources/pic18/plib/USART/u2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1 sources/pic18/plib/USART/u2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1: sources/pic18/plib/USART/u2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1 sources/pic18/plib/USART/u2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.d ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1: sources/pic18/plib/USART/u2putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1 sources/pic18/plib/USART/u2putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1: sources/pic18/plib/USART/u2puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1 sources/pic18/plib/USART/u2puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1: sources/pic18/plib/USART/u2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1 sources/pic18/plib/USART/u2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.d ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1: sources/pic18/plib/USART/u2write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1 sources/pic18/plib/USART/u2write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.d ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1: sources/pic18/plib/USART/u3baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1 sources/pic18/plib/USART/u3baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1: sources/pic18/plib/USART/u3busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1 sources/pic18/plib/USART/u3busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1: sources/pic18/plib/USART/u3close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1 sources/pic18/plib/USART/u3close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.d ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1: sources/pic18/plib/USART/u3defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1 sources/pic18/plib/USART/u3defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1: sources/pic18/plib/USART/u3drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1 sources/pic18/plib/USART/u3drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1: sources/pic18/plib/USART/u3gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1 sources/pic18/plib/USART/u3gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1: sources/pic18/plib/USART/u3open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1 sources/pic18/plib/USART/u3open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.d ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1: sources/pic18/plib/USART/u3putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1 sources/pic18/plib/USART/u3putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1: sources/pic18/plib/USART/u3puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1 sources/pic18/plib/USART/u3puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1: sources/pic18/plib/USART/u3read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1 sources/pic18/plib/USART/u3read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.d ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1: sources/pic18/plib/USART/u3write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1 sources/pic18/plib/USART/u3write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.d ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1: sources/pic18/plib/USART/u4baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1 sources/pic18/plib/USART/u4baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1: sources/pic18/plib/USART/u4busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1 sources/pic18/plib/USART/u4busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1: sources/pic18/plib/USART/u4close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1 sources/pic18/plib/USART/u4close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.d ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1: sources/pic18/plib/USART/u4defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1 sources/pic18/plib/USART/u4defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1: sources/pic18/plib/USART/u4drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1 sources/pic18/plib/USART/u4drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1: sources/pic18/plib/USART/u4gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1 sources/pic18/plib/USART/u4gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1: sources/pic18/plib/USART/u4open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1 sources/pic18/plib/USART/u4open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.d ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1: sources/pic18/plib/USART/u4putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1 sources/pic18/plib/USART/u4putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1: sources/pic18/plib/USART/u4puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1 sources/pic18/plib/USART/u4puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1: sources/pic18/plib/USART/u4read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1 sources/pic18/plib/USART/u4read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.d ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1: sources/pic18/plib/USART/u4write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1 sources/pic18/plib/USART/u4write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.d ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1: sources/pic18/plib/USART/ubaud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1 sources/pic18/plib/USART/ubaud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.d ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1: sources/pic18/plib/USART/ubusy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1 sources/pic18/plib/USART/ubusy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.d ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1: sources/pic18/plib/USART/uclose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1 sources/pic18/plib/USART/uclose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.d ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1: sources/pic18/plib/USART/udefs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1 sources/pic18/plib/USART/udefs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.d ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1: sources/pic18/plib/USART/udrdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1 sources/pic18/plib/USART/udrdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.d ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1: sources/pic18/plib/USART/ugets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1 sources/pic18/plib/USART/ugets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.d ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1: sources/pic18/plib/USART/uopen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1 sources/pic18/plib/USART/uopen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.d ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1: sources/pic18/plib/USART/uputrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1 sources/pic18/plib/USART/uputrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.d ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1: sources/pic18/plib/USART/uputs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1 sources/pic18/plib/USART/uputs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.d ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uread.p1: sources/pic18/plib/USART/uread.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1 sources/pic18/plib/USART/uread.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uread.d ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1: sources/pic18/plib/USART/uwrite.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1 sources/pic18/plib/USART/uwrite.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.d ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1: sources/pic18/plib/XLCD/busyxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1 sources/pic18/plib/XLCD/busyxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1: sources/pic18/plib/XLCD/openxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1 sources/pic18/plib/XLCD/openxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1: sources/pic18/plib/XLCD/putrxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1 sources/pic18/plib/XLCD/putrxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1: sources/pic18/plib/XLCD/putsxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1 sources/pic18/plib/XLCD/putsxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1: sources/pic18/plib/XLCD/readaddr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1 sources/pic18/plib/XLCD/readaddr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.d ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1: sources/pic18/plib/XLCD/readdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1 sources/pic18/plib/XLCD/readdata.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.d ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1: sources/pic18/plib/XLCD/setcgram.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1 sources/pic18/plib/XLCD/setcgram.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.d ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1: sources/pic18/plib/XLCD/setddram.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1 sources/pic18/plib/XLCD/setddram.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.d ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1: sources/pic18/plib/XLCD/wcmdxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1 sources/pic18/plib/XLCD/wcmdxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1: sources/pic18/plib/XLCD/writdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1 sources/pic18/plib/XLCD/writdata.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.d ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c  -D__DEBUG=1  -mdebugger=none   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1: sources/pic18/plib/ADC/adcbusy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1 sources/pic18/plib/ADC/adcbusy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcbusy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1: sources/pic18/plib/ADC/adcclose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1 sources/pic18/plib/ADC/adcclose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcclose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1: sources/pic18/plib/ADC/adcconv.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1 sources/pic18/plib/ADC/adcconv.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcconv.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1: sources/pic18/plib/ADC/adcopen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1 sources/pic18/plib/ADC/adcopen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcopen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1: sources/pic18/plib/ADC/adcread.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1 sources/pic18/plib/ADC/adcread.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcread.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1: sources/pic18/plib/ADC/adcselchconv.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1 sources/pic18/plib/ADC/adcselchconv.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcselchconv.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1: sources/pic18/plib/ADC/adcsetch.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ADC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1 sources/pic18/plib/ADC/adcsetch.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.d ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ADC/adcsetch.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1: sources/pic18/plib/ancom/close_ancomp.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ancom" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1 sources/pic18/plib/ancom/close_ancomp.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.d ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ancom/close_ancomp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1: sources/pic18/plib/ancom/open_ancomp.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/ancom" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1 sources/pic18/plib/ancom/open_ancomp.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.d ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/ancom/open_ancomp.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1: sources/pic18/plib/CTMU/CloseCTMU.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/CTMU" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1 sources/pic18/plib/CTMU/CloseCTMU.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.d ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/CTMU/CloseCTMU.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1: sources/pic18/plib/CTMU/CurrentControlCTMU.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/CTMU" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1 sources/pic18/plib/CTMU/CurrentControlCTMU.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.d ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/CTMU/CurrentControlCTMU.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1: sources/pic18/plib/CTMU/OpenCTMU.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/CTMU" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1 sources/pic18/plib/CTMU/OpenCTMU.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.d ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/CTMU/OpenCTMU.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1: sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1 sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/DeepSleepWakeUpSource.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1: sources/pic18/plib/DPSLP/GotoDeepSleep.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1 sources/pic18/plib/DPSLP/GotoDeepSleep.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/GotoDeepSleep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1: sources/pic18/plib/DPSLP/IsResetFromDeepSleep.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1 sources/pic18/plib/DPSLP/IsResetFromDeepSleep.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/IsResetFromDeepSleep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1: sources/pic18/plib/DPSLP/ReadDSGPR.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1 sources/pic18/plib/DPSLP/ReadDSGPR.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ReadDSGPR.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1: sources/pic18/plib/DPSLP/ULPWakeUpEnable.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/DPSLP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1 sources/pic18/plib/DPSLP/ULPWakeUpEnable.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.d ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/DPSLP/ULPWakeUpEnable.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1: sources/pic18/plib/EEP/busy_eep.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/EEP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1 sources/pic18/plib/EEP/busy_eep.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.d ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/EEP/busy_eep.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1: sources/pic18/plib/EEP/read_B.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/EEP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1 sources/pic18/plib/EEP/read_B.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.d ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/EEP/read_B.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1: sources/pic18/plib/EEP/write_B.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/EEP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1 sources/pic18/plib/EEP/write_B.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.d ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/EEP/write_B.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1: sources/pic18/plib/Flash/EraseFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1 sources/pic18/plib/Flash/EraseFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/EraseFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1: sources/pic18/plib/Flash/ReadFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1 sources/pic18/plib/Flash/ReadFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/ReadFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1: sources/pic18/plib/Flash/WriteBlockFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1 sources/pic18/plib/Flash/WriteBlockFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBlockFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1: sources/pic18/plib/Flash/WriteBytesFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1 sources/pic18/plib/Flash/WriteBytesFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteBytesFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1: sources/pic18/plib/Flash/WriteWordFlash.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Flash" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1 sources/pic18/plib/Flash/WriteWordFlash.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.d ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Flash/WriteWordFlash.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1: sources/pic18/plib/i2c/i2c1ack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1 sources/pic18/plib/i2c/i2c1ack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1: sources/pic18/plib/i2c/i2c1clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1 sources/pic18/plib/i2c/i2c1clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1: sources/pic18/plib/i2c/i2c1dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1 sources/pic18/plib/i2c/i2c1dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1: sources/pic18/plib/i2c/i2c1eeap.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1 sources/pic18/plib/i2c/i2c1eeap.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1: sources/pic18/plib/i2c/i2c1eebw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1 sources/pic18/plib/i2c/i2c1eebw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1: sources/pic18/plib/i2c/i2c1eecr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1 sources/pic18/plib/i2c/i2c1eecr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1: sources/pic18/plib/i2c/i2c1eepw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1 sources/pic18/plib/i2c/i2c1eepw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1: sources/pic18/plib/i2c/i2c1eerr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1 sources/pic18/plib/i2c/i2c1eerr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1: sources/pic18/plib/i2c/i2c1eesr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1 sources/pic18/plib/i2c/i2c1eesr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1: sources/pic18/plib/i2c/i2c1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1 sources/pic18/plib/i2c/i2c1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1: sources/pic18/plib/i2c/i2c1idle.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1 sources/pic18/plib/i2c/i2c1idle.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1: sources/pic18/plib/i2c/i2c1nack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1 sources/pic18/plib/i2c/i2c1nack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1: sources/pic18/plib/i2c/i2c1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1 sources/pic18/plib/i2c/i2c1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1: sources/pic18/plib/i2c/i2c1puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1 sources/pic18/plib/i2c/i2c1puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1: sources/pic18/plib/i2c/i2c1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1 sources/pic18/plib/i2c/i2c1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1: sources/pic18/plib/i2c/i2c1rstr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1 sources/pic18/plib/i2c/i2c1rstr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1: sources/pic18/plib/i2c/i2c1stop.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1 sources/pic18/plib/i2c/i2c1stop.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1: sources/pic18/plib/i2c/i2c1strt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1 sources/pic18/plib/i2c/i2c1strt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1: sources/pic18/plib/i2c/i2c1writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1 sources/pic18/plib/i2c/i2c1writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c1writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1: sources/pic18/plib/i2c/i2c2ack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1 sources/pic18/plib/i2c/i2c2ack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1: sources/pic18/plib/i2c/i2c2clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1 sources/pic18/plib/i2c/i2c2clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1: sources/pic18/plib/i2c/i2c2dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1 sources/pic18/plib/i2c/i2c2dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1: sources/pic18/plib/i2c/i2c2eeap.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1 sources/pic18/plib/i2c/i2c2eeap.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1: sources/pic18/plib/i2c/i2c2eebw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1 sources/pic18/plib/i2c/i2c2eebw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1: sources/pic18/plib/i2c/i2c2eecr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1 sources/pic18/plib/i2c/i2c2eecr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1: sources/pic18/plib/i2c/i2c2eepw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1 sources/pic18/plib/i2c/i2c2eepw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1: sources/pic18/plib/i2c/i2c2eerr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1 sources/pic18/plib/i2c/i2c2eerr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1: sources/pic18/plib/i2c/i2c2eesr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1 sources/pic18/plib/i2c/i2c2eesr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1: sources/pic18/plib/i2c/i2c2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1 sources/pic18/plib/i2c/i2c2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1: sources/pic18/plib/i2c/i2c2idle.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1 sources/pic18/plib/i2c/i2c2idle.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1: sources/pic18/plib/i2c/i2c2nack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1 sources/pic18/plib/i2c/i2c2nack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1: sources/pic18/plib/i2c/i2c2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1 sources/pic18/plib/i2c/i2c2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1: sources/pic18/plib/i2c/i2c2puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1 sources/pic18/plib/i2c/i2c2puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1: sources/pic18/plib/i2c/i2c2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1 sources/pic18/plib/i2c/i2c2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1: sources/pic18/plib/i2c/i2c2rstr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1 sources/pic18/plib/i2c/i2c2rstr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1: sources/pic18/plib/i2c/i2c2stop.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1 sources/pic18/plib/i2c/i2c2stop.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1: sources/pic18/plib/i2c/i2c2strt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1 sources/pic18/plib/i2c/i2c2strt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1: sources/pic18/plib/i2c/i2c2writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1 sources/pic18/plib/i2c/i2c2writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c2writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1: sources/pic18/plib/i2c/i2c_ack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1 sources/pic18/plib/i2c/i2c_ack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_ack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1: sources/pic18/plib/i2c/i2c_clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1 sources/pic18/plib/i2c/i2c_clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1: sources/pic18/plib/i2c/i2c_dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1 sources/pic18/plib/i2c/i2c_dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1: sources/pic18/plib/i2c/i2c_eeap.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1 sources/pic18/plib/i2c/i2c_eeap.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eeap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1: sources/pic18/plib/i2c/i2c_eebw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1 sources/pic18/plib/i2c/i2c_eebw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eebw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1: sources/pic18/plib/i2c/i2c_eecr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1 sources/pic18/plib/i2c/i2c_eecr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eecr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1: sources/pic18/plib/i2c/i2c_eepw.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1 sources/pic18/plib/i2c/i2c_eepw.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eepw.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1: sources/pic18/plib/i2c/i2c_eerr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1 sources/pic18/plib/i2c/i2c_eerr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eerr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1: sources/pic18/plib/i2c/i2c_eesr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1 sources/pic18/plib/i2c/i2c_eesr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_eesr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1: sources/pic18/plib/i2c/i2c_gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1 sources/pic18/plib/i2c/i2c_gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1: sources/pic18/plib/i2c/i2c_idle.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1 sources/pic18/plib/i2c/i2c_idle.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_idle.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1: sources/pic18/plib/i2c/i2c_nack.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1 sources/pic18/plib/i2c/i2c_nack.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_nack.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1: sources/pic18/plib/i2c/i2c_open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1 sources/pic18/plib/i2c/i2c_open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1: sources/pic18/plib/i2c/i2c_puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1 sources/pic18/plib/i2c/i2c_puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1: sources/pic18/plib/i2c/i2c_read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1 sources/pic18/plib/i2c/i2c_read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1: sources/pic18/plib/i2c/i2c_rstr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1 sources/pic18/plib/i2c/i2c_rstr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_rstr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1: sources/pic18/plib/i2c/i2c_stop.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1 sources/pic18/plib/i2c/i2c_stop.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_stop.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1: sources/pic18/plib/i2c/i2c_strt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1 sources/pic18/plib/i2c/i2c_strt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_strt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1: sources/pic18/plib/i2c/i2c_writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/i2c" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1 sources/pic18/plib/i2c/i2c_writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.d ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/i2c/i2c_writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1: sources/pic18/plib/mwire/mw1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1 sources/pic18/plib/mwire/mw1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1: sources/pic18/plib/mwire/mw1drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1 sources/pic18/plib/mwire/mw1drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1: sources/pic18/plib/mwire/mw1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1 sources/pic18/plib/mwire/mw1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1: sources/pic18/plib/mwire/mw1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1 sources/pic18/plib/mwire/mw1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1: sources/pic18/plib/mwire/mw1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1 sources/pic18/plib/mwire/mw1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1: sources/pic18/plib/mwire/mw1write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1 sources/pic18/plib/mwire/mw1write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw1write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1: sources/pic18/plib/mwire/mw2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1 sources/pic18/plib/mwire/mw2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1: sources/pic18/plib/mwire/mw2drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1 sources/pic18/plib/mwire/mw2drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1: sources/pic18/plib/mwire/mw2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1 sources/pic18/plib/mwire/mw2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1: sources/pic18/plib/mwire/mw2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1 sources/pic18/plib/mwire/mw2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1: sources/pic18/plib/mwire/mw2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1 sources/pic18/plib/mwire/mw2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1: sources/pic18/plib/mwire/mw2write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1 sources/pic18/plib/mwire/mw2write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw2write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1: sources/pic18/plib/mwire/mw_close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1 sources/pic18/plib/mwire/mw_close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1: sources/pic18/plib/mwire/mw_drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1 sources/pic18/plib/mwire/mw_drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1: sources/pic18/plib/mwire/mw_gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1 sources/pic18/plib/mwire/mw_gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1: sources/pic18/plib/mwire/mw_open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1 sources/pic18/plib/mwire/mw_open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1: sources/pic18/plib/mwire/mw_read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1 sources/pic18/plib/mwire/mw_read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1: sources/pic18/plib/mwire/mw_write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/mwire" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1 sources/pic18/plib/mwire/mw_write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.d ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/mwire/mw_write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1: sources/pic18/plib/pcpwm/close_pcpwm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1 sources/pic18/plib/pcpwm/close_pcpwm.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/close_pcpwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1: sources/pic18/plib/pcpwm/open_pcpwm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1 sources/pic18/plib/pcpwm/open_pcpwm.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/open_pcpwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1: sources/pic18/plib/pcpwm/pcpwm_dt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1 sources/pic18/plib/pcpwm/pcpwm_dt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_dt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1: sources/pic18/plib/pcpwm/pcpwm_OVD.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1 sources/pic18/plib/pcpwm/pcpwm_OVD.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/pcpwm_OVD.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1: sources/pic18/plib/pcpwm/set_dcpcpwm.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/pcpwm" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1 sources/pic18/plib/pcpwm/set_dcpcpwm.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.d ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/pcpwm/set_dcpcpwm.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1: sources/pic18/plib/PMP/PMPClose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1 sources/pic18/plib/PMP/PMPClose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPClose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1: sources/pic18/plib/PMP/PMPIsBUfferNEmpty.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1 sources/pic18/plib/PMP/PMPIsBUfferNEmpty.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBUfferNEmpty.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1: sources/pic18/plib/PMP/PMPIsBufferNFull.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1 sources/pic18/plib/PMP/PMPIsBufferNFull.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPIsBufferNFull.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1: sources/pic18/plib/PMP/PMPMasterRead.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1 sources/pic18/plib/PMP/PMPMasterRead.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterRead.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1: sources/pic18/plib/PMP/PMPMasterWrite.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1 sources/pic18/plib/PMP/PMPMasterWrite.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPMasterWrite.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1: sources/pic18/plib/PMP/PMPOpen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1 sources/pic18/plib/PMP/PMPOpen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPOpen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1: sources/pic18/plib/PMP/PMPSetAddress.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1 sources/pic18/plib/PMP/PMPSetAddress.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSetAddress.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1: sources/pic18/plib/PMP/PMPSlaveReadBufferN.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1 sources/pic18/plib/PMP/PMPSlaveReadBufferN.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBufferN.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1: sources/pic18/plib/PMP/PMPSlaveReadBuffers.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1 sources/pic18/plib/PMP/PMPSlaveReadBuffers.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveReadBuffers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1: sources/pic18/plib/PMP/PMPSlaveWriteBufferN.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1 sources/pic18/plib/PMP/PMPSlaveWriteBufferN.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBufferN.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1: sources/pic18/plib/PMP/PMPSlaveWriteBuffers.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PMP" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1 sources/pic18/plib/PMP/PMPSlaveWriteBuffers.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.d ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PMP/PMPSlaveWriteBuffers.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1: sources/pic18/plib/PORTB/pbclose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1 sources/pic18/plib/PORTB/pbclose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbclose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1: sources/pic18/plib/PORTB/pbopen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1 sources/pic18/plib/PORTB/pbopen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pbopen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1: sources/pic18/plib/PORTB/pulldis.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1 sources/pic18/plib/PORTB/pulldis.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pulldis.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1: sources/pic18/plib/PORTB/pullen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1 sources/pic18/plib/PORTB/pullen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.d ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/pullen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1: sources/pic18/plib/PORTB/rb0close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1 sources/pic18/plib/PORTB/rb0close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1: sources/pic18/plib/PORTB/rb0open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1 sources/pic18/plib/PORTB/rb0open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb0open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1: sources/pic18/plib/PORTB/rb1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1 sources/pic18/plib/PORTB/rb1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1: sources/pic18/plib/PORTB/rb1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1 sources/pic18/plib/PORTB/rb1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1: sources/pic18/plib/PORTB/rb2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1 sources/pic18/plib/PORTB/rb2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1: sources/pic18/plib/PORTB/rb2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1 sources/pic18/plib/PORTB/rb2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1: sources/pic18/plib/PORTB/rb3close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1 sources/pic18/plib/PORTB/rb3close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1: sources/pic18/plib/PORTB/rb3open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/PORTB" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1 sources/pic18/plib/PORTB/rb3open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.d ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/PORTB/rb3open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1: sources/pic18/plib/rtcc/RTCCInitClock.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1 sources/pic18/plib/rtcc/RTCCInitClock.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RTCCInitClock.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1: sources/pic18/plib/rtcc/RtccReadAlrmDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1 sources/pic18/plib/rtcc/RtccReadAlrmDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1: sources/pic18/plib/rtcc/RtccReadAlrmTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1 sources/pic18/plib/rtcc/RtccReadAlrmTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1: sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1 sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadAlrmTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1: sources/pic18/plib/rtcc/RtccReadDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1 sources/pic18/plib/rtcc/RtccReadDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1: sources/pic18/plib/rtcc/RtccReadTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1 sources/pic18/plib/rtcc/RtccReadTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1: sources/pic18/plib/rtcc/RtccReadTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1 sources/pic18/plib/rtcc/RtccReadTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccReadTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1: sources/pic18/plib/rtcc/RtccSetAlarmRpt.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1 sources/pic18/plib/rtcc/RtccSetAlarmRpt.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRpt.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1: sources/pic18/plib/rtcc/RtccSetAlarmRptCount.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1 sources/pic18/plib/rtcc/RtccSetAlarmRptCount.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetAlarmRptCount.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1: sources/pic18/plib/rtcc/RtccSetCalibration.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1 sources/pic18/plib/rtcc/RtccSetCalibration.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetCalibration.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1: sources/pic18/plib/rtcc/RtccSetChimeEnbl.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1 sources/pic18/plib/rtcc/RtccSetChimeEnbl.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccSetChimeEnbl.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1: sources/pic18/plib/rtcc/RtccWriteAlrmDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1 sources/pic18/plib/rtcc/RtccWriteAlrmDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1: sources/pic18/plib/rtcc/RtccWriteAlrmTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1 sources/pic18/plib/rtcc/RtccWriteAlrmTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1: sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1 sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteAlrmTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1: sources/pic18/plib/rtcc/RtccWriteDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1 sources/pic18/plib/rtcc/RtccWriteDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1: sources/pic18/plib/rtcc/RtccWriteTime.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1 sources/pic18/plib/rtcc/RtccWriteTime.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTime.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1: sources/pic18/plib/rtcc/RtccWriteTimeDate.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1 sources/pic18/plib/rtcc/RtccWriteTimeDate.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWriteTimeDate.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1: sources/pic18/plib/rtcc/RtccWrOn.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/rtcc" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1 sources/pic18/plib/rtcc/RtccWrOn.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.d ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/rtcc/RtccWrOn.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1: sources/pic18/plib/SPI/spi1clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1 sources/pic18/plib/SPI/spi1clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1: sources/pic18/plib/SPI/spi1dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1 sources/pic18/plib/SPI/spi1dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1: sources/pic18/plib/SPI/spi1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1 sources/pic18/plib/SPI/spi1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1: sources/pic18/plib/SPI/spi1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1 sources/pic18/plib/SPI/spi1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1: sources/pic18/plib/SPI/spi1puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1 sources/pic18/plib/SPI/spi1puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1: sources/pic18/plib/SPI/spi1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1 sources/pic18/plib/SPI/spi1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1: sources/pic18/plib/SPI/spi1writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1 sources/pic18/plib/SPI/spi1writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi1writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1: sources/pic18/plib/SPI/spi2clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1 sources/pic18/plib/SPI/spi2clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1: sources/pic18/plib/SPI/spi2dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1 sources/pic18/plib/SPI/spi2dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1: sources/pic18/plib/SPI/spi2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1 sources/pic18/plib/SPI/spi2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1: sources/pic18/plib/SPI/spi2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1 sources/pic18/plib/SPI/spi2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1: sources/pic18/plib/SPI/spi2puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1 sources/pic18/plib/SPI/spi2puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1: sources/pic18/plib/SPI/spi2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1 sources/pic18/plib/SPI/spi2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1: sources/pic18/plib/SPI/spi2writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1 sources/pic18/plib/SPI/spi2writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi2writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1: sources/pic18/plib/SPI/spi_clos.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1 sources/pic18/plib/SPI/spi_clos.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_clos.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1: sources/pic18/plib/SPI/spi_dtrd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1 sources/pic18/plib/SPI/spi_dtrd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_dtrd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1: sources/pic18/plib/SPI/spi_gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1 sources/pic18/plib/SPI/spi_gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1: sources/pic18/plib/SPI/spi_open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1 sources/pic18/plib/SPI/spi_open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1: sources/pic18/plib/SPI/spi_puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1 sources/pic18/plib/SPI/spi_puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1: sources/pic18/plib/SPI/spi_read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1 sources/pic18/plib/SPI/spi_read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1: sources/pic18/plib/SPI/spi_writ.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1 sources/pic18/plib/SPI/spi_writ.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.d ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SPI/spi_writ.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1: sources/pic18/plib/SW_I2C/swacki2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1 sources/pic18/plib/SW_I2C/swacki2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swacki2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1: sources/pic18/plib/SW_I2C/swckti2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1 sources/pic18/plib/SW_I2C/swckti2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swckti2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1: sources/pic18/plib/SW_I2C/swgtci2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1 sources/pic18/plib/SW_I2C/swgtci2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtci2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1: sources/pic18/plib/SW_I2C/swgtsi2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1 sources/pic18/plib/SW_I2C/swgtsi2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swgtsi2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1: sources/pic18/plib/SW_I2C/swptci2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1 sources/pic18/plib/SW_I2C/swptci2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptci2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1: sources/pic18/plib/SW_I2C/swptsi2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1 sources/pic18/plib/SW_I2C/swptsi2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swptsi2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1: sources/pic18/plib/SW_I2C/swrsti2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1 sources/pic18/plib/SW_I2C/swrsti2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swrsti2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1: sources/pic18/plib/SW_I2C/swstpi2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1 sources/pic18/plib/SW_I2C/swstpi2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstpi2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1: sources/pic18/plib/SW_I2C/swstri2c.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_I2C" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1 sources/pic18/plib/SW_I2C/swstri2c.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.d ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_I2C/swstri2c.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1: sources/pic18/plib/SW_RTCC/close_rtcc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_RTCC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1 sources/pic18/plib/SW_RTCC/close_rtcc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/close_rtcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1: sources/pic18/plib/SW_RTCC/open_rtcc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_RTCC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1 sources/pic18/plib/SW_RTCC/open_rtcc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/open_rtcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1: sources/pic18/plib/SW_RTCC/update_rtcc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_RTCC" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1 sources/pic18/plib/SW_RTCC/update_rtcc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.d ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_RTCC/update_rtcc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1: sources/pic18/plib/SW_SPI/clrcsspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1 sources/pic18/plib/SW_SPI/clrcsspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/clrcsspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1: sources/pic18/plib/SW_SPI/opensspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1 sources/pic18/plib/SW_SPI/opensspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/opensspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1: sources/pic18/plib/SW_SPI/setcsspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1 sources/pic18/plib/SW_SPI/setcsspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/setcsspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1: sources/pic18/plib/SW_SPI/wrtsspi.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_SPI" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1 sources/pic18/plib/SW_SPI/wrtsspi.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.d ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_SPI/wrtsspi.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1: sources/pic18/plib/SW_UART/openuart.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_UART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1 sources/pic18/plib/SW_UART/openuart.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_UART/openuart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1: sources/pic18/plib/SW_UART/sw_uart.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_UART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1 sources/pic18/plib/SW_UART/sw_uart.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_UART/sw_uart.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1: sources/pic18/plib/SW_UART/uartdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/SW_UART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1 sources/pic18/plib/SW_UART/uartdata.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.d ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/SW_UART/uartdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1: sources/pic18/plib/Timers/t0close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1 sources/pic18/plib/Timers/t0close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1: sources/pic18/plib/Timers/t0open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1 sources/pic18/plib/Timers/t0open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1: sources/pic18/plib/Timers/t0read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1 sources/pic18/plib/Timers/t0read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1: sources/pic18/plib/Timers/t0write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1 sources/pic18/plib/Timers/t0write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t0write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1: sources/pic18/plib/Timers/t10close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1 sources/pic18/plib/Timers/t10close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1: sources/pic18/plib/Timers/t10open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1 sources/pic18/plib/Timers/t10open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1: sources/pic18/plib/Timers/t10read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1 sources/pic18/plib/Timers/t10read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1: sources/pic18/plib/Timers/t10write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1 sources/pic18/plib/Timers/t10write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t10write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1: sources/pic18/plib/Timers/t12close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1 sources/pic18/plib/Timers/t12close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1: sources/pic18/plib/Timers/t12open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1 sources/pic18/plib/Timers/t12open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1: sources/pic18/plib/Timers/t12read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1 sources/pic18/plib/Timers/t12read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1: sources/pic18/plib/Timers/t12write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1 sources/pic18/plib/Timers/t12write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t12write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1: sources/pic18/plib/Timers/t1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1 sources/pic18/plib/Timers/t1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1: sources/pic18/plib/Timers/t1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1 sources/pic18/plib/Timers/t1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1: sources/pic18/plib/Timers/t1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1 sources/pic18/plib/Timers/t1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1: sources/pic18/plib/Timers/t1write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1 sources/pic18/plib/Timers/t1write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t1write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1: sources/pic18/plib/Timers/t2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1 sources/pic18/plib/Timers/t2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1: sources/pic18/plib/Timers/t2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1 sources/pic18/plib/Timers/t2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1: sources/pic18/plib/Timers/t2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1 sources/pic18/plib/Timers/t2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1: sources/pic18/plib/Timers/t2write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1 sources/pic18/plib/Timers/t2write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t2write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1: sources/pic18/plib/Timers/t3close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1 sources/pic18/plib/Timers/t3close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1: sources/pic18/plib/Timers/t3open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1 sources/pic18/plib/Timers/t3open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1: sources/pic18/plib/Timers/t3read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1 sources/pic18/plib/Timers/t3read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1: sources/pic18/plib/Timers/t3write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1 sources/pic18/plib/Timers/t3write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t3write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1: sources/pic18/plib/Timers/t4close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1 sources/pic18/plib/Timers/t4close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1: sources/pic18/plib/Timers/t4open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1 sources/pic18/plib/Timers/t4open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1: sources/pic18/plib/Timers/t4read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1 sources/pic18/plib/Timers/t4read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1: sources/pic18/plib/Timers/t4write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1 sources/pic18/plib/Timers/t4write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t4write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1: sources/pic18/plib/Timers/t5close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1 sources/pic18/plib/Timers/t5close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1: sources/pic18/plib/Timers/t5open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1 sources/pic18/plib/Timers/t5open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1: sources/pic18/plib/Timers/t5read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1 sources/pic18/plib/Timers/t5read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1: sources/pic18/plib/Timers/t5write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1 sources/pic18/plib/Timers/t5write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t5write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1: sources/pic18/plib/Timers/t6close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1 sources/pic18/plib/Timers/t6close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1: sources/pic18/plib/Timers/t6open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1 sources/pic18/plib/Timers/t6open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1: sources/pic18/plib/Timers/t6read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1 sources/pic18/plib/Timers/t6read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1: sources/pic18/plib/Timers/t6write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1 sources/pic18/plib/Timers/t6write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t6write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1: sources/pic18/plib/Timers/t7close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1 sources/pic18/plib/Timers/t7close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1: sources/pic18/plib/Timers/t7open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1 sources/pic18/plib/Timers/t7open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1: sources/pic18/plib/Timers/t7read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1 sources/pic18/plib/Timers/t7read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1: sources/pic18/plib/Timers/t7write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1 sources/pic18/plib/Timers/t7write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t7write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1: sources/pic18/plib/Timers/t8close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1 sources/pic18/plib/Timers/t8close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1: sources/pic18/plib/Timers/t8open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1 sources/pic18/plib/Timers/t8open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1: sources/pic18/plib/Timers/t8read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1 sources/pic18/plib/Timers/t8read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1: sources/pic18/plib/Timers/t8write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1 sources/pic18/plib/Timers/t8write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.d ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/t8write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1: sources/pic18/plib/Timers/tmrccpsrc.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/Timers" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1 sources/pic18/plib/Timers/tmrccpsrc.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.d ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/Timers/tmrccpsrc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1: sources/pic18/plib/USART/u1baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1 sources/pic18/plib/USART/u1baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1: sources/pic18/plib/USART/u1busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1 sources/pic18/plib/USART/u1busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1: sources/pic18/plib/USART/u1close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1 sources/pic18/plib/USART/u1close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.d ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1: sources/pic18/plib/USART/u1defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1 sources/pic18/plib/USART/u1defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1: sources/pic18/plib/USART/u1drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1 sources/pic18/plib/USART/u1drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1: sources/pic18/plib/USART/u1gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1 sources/pic18/plib/USART/u1gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1: sources/pic18/plib/USART/u1open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1 sources/pic18/plib/USART/u1open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.d ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1: sources/pic18/plib/USART/u1putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1 sources/pic18/plib/USART/u1putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1: sources/pic18/plib/USART/u1puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1 sources/pic18/plib/USART/u1puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1: sources/pic18/plib/USART/u1read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1 sources/pic18/plib/USART/u1read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.d ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1: sources/pic18/plib/USART/u1write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1 sources/pic18/plib/USART/u1write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.d ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u1write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1: sources/pic18/plib/USART/u2baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1 sources/pic18/plib/USART/u2baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1: sources/pic18/plib/USART/u2busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1 sources/pic18/plib/USART/u2busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1: sources/pic18/plib/USART/u2close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1 sources/pic18/plib/USART/u2close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.d ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1: sources/pic18/plib/USART/u2defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1 sources/pic18/plib/USART/u2defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1: sources/pic18/plib/USART/u2drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1 sources/pic18/plib/USART/u2drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1: sources/pic18/plib/USART/u2gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1 sources/pic18/plib/USART/u2gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1: sources/pic18/plib/USART/u2open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1 sources/pic18/plib/USART/u2open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.d ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1: sources/pic18/plib/USART/u2putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1 sources/pic18/plib/USART/u2putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1: sources/pic18/plib/USART/u2puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1 sources/pic18/plib/USART/u2puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1: sources/pic18/plib/USART/u2read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1 sources/pic18/plib/USART/u2read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.d ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1: sources/pic18/plib/USART/u2write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1 sources/pic18/plib/USART/u2write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.d ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u2write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1: sources/pic18/plib/USART/u3baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1 sources/pic18/plib/USART/u3baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1: sources/pic18/plib/USART/u3busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1 sources/pic18/plib/USART/u3busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1: sources/pic18/plib/USART/u3close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1 sources/pic18/plib/USART/u3close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.d ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1: sources/pic18/plib/USART/u3defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1 sources/pic18/plib/USART/u3defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1: sources/pic18/plib/USART/u3drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1 sources/pic18/plib/USART/u3drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1: sources/pic18/plib/USART/u3gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1 sources/pic18/plib/USART/u3gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1: sources/pic18/plib/USART/u3open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1 sources/pic18/plib/USART/u3open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.d ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1: sources/pic18/plib/USART/u3putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1 sources/pic18/plib/USART/u3putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1: sources/pic18/plib/USART/u3puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1 sources/pic18/plib/USART/u3puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1: sources/pic18/plib/USART/u3read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1 sources/pic18/plib/USART/u3read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.d ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1: sources/pic18/plib/USART/u3write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1 sources/pic18/plib/USART/u3write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.d ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u3write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1: sources/pic18/plib/USART/u4baud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1 sources/pic18/plib/USART/u4baud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.d ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4baud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1: sources/pic18/plib/USART/u4busy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1 sources/pic18/plib/USART/u4busy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.d ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4busy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1: sources/pic18/plib/USART/u4close.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1 sources/pic18/plib/USART/u4close.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.d ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4close.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1: sources/pic18/plib/USART/u4defs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1 sources/pic18/plib/USART/u4defs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.d ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4defs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1: sources/pic18/plib/USART/u4drdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1 sources/pic18/plib/USART/u4drdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.d ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4drdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1: sources/pic18/plib/USART/u4gets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1 sources/pic18/plib/USART/u4gets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.d ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4gets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1: sources/pic18/plib/USART/u4open.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1 sources/pic18/plib/USART/u4open.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.d ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4open.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1: sources/pic18/plib/USART/u4putrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1 sources/pic18/plib/USART/u4putrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.d ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4putrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1: sources/pic18/plib/USART/u4puts.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1 sources/pic18/plib/USART/u4puts.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.d ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4puts.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1: sources/pic18/plib/USART/u4read.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1 sources/pic18/plib/USART/u4read.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.d ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4read.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1: sources/pic18/plib/USART/u4write.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1 sources/pic18/plib/USART/u4write.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.d ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/u4write.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1: sources/pic18/plib/USART/ubaud.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1 sources/pic18/plib/USART/ubaud.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.d ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/ubaud.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1: sources/pic18/plib/USART/ubusy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1 sources/pic18/plib/USART/ubusy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.d ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/ubusy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1: sources/pic18/plib/USART/uclose.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1 sources/pic18/plib/USART/uclose.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.d ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uclose.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1: sources/pic18/plib/USART/udefs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1 sources/pic18/plib/USART/udefs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.d ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/udefs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1: sources/pic18/plib/USART/udrdy.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1 sources/pic18/plib/USART/udrdy.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.d ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/udrdy.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1: sources/pic18/plib/USART/ugets.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1 sources/pic18/plib/USART/ugets.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.d ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/ugets.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1: sources/pic18/plib/USART/uopen.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1 sources/pic18/plib/USART/uopen.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.d ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uopen.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1: sources/pic18/plib/USART/uputrs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1 sources/pic18/plib/USART/uputrs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.d ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uputrs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1: sources/pic18/plib/USART/uputs.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1 sources/pic18/plib/USART/uputs.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.d ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uputs.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uread.p1: sources/pic18/plib/USART/uread.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1 sources/pic18/plib/USART/uread.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uread.d ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uread.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1: sources/pic18/plib/USART/uwrite.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/USART" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1 sources/pic18/plib/USART/uwrite.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.d ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/USART/uwrite.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1: sources/pic18/plib/XLCD/busyxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1 sources/pic18/plib/XLCD/busyxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/busyxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1: sources/pic18/plib/XLCD/openxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1 sources/pic18/plib/XLCD/openxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/openxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1: sources/pic18/plib/XLCD/putrxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1 sources/pic18/plib/XLCD/putrxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/putrxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1: sources/pic18/plib/XLCD/putsxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1 sources/pic18/plib/XLCD/putsxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/putsxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1: sources/pic18/plib/XLCD/readaddr.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1 sources/pic18/plib/XLCD/readaddr.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.d ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/readaddr.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1: sources/pic18/plib/XLCD/readdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1 sources/pic18/plib/XLCD/readdata.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.d ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/readdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1: sources/pic18/plib/XLCD/setcgram.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1 sources/pic18/plib/XLCD/setcgram.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.d ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/setcgram.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1: sources/pic18/plib/XLCD/setddram.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1 sources/pic18/plib/XLCD/setddram.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.d ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/setddram.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1: sources/pic18/plib/XLCD/wcmdxlcd.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1 sources/pic18/plib/XLCD/wcmdxlcd.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.d ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/wcmdxlcd.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1: sources/pic18/plib/XLCD/writdata.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}/sources/pic18/plib/XLCD" 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1.d 
	@${RM} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1 sources/pic18/plib/XLCD/writdata.c 
	@-${MV} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.d ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/sources/pic18/plib/XLCD/writdata.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk 
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} $(MP_EXTRA_CC_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -c   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -DXPRJ_default=$(CND_CONF)  -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits $(COMPARISON_BUILD)  -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     -o ${OBJECTDIR}/main.p1 main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.map  -D__DEBUG=1  -mdebugger=none  -DXPRJ_default=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto        $(COMPARISON_BUILD) -Wl,--memorysummary,${DISTDIR}/memoryfile.xml -o ${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} ${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.hex 
	
else
${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} ${DISTDIR} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -mcpu=$(MP_PROCESSOR_OPTION) -Wl,-Map=${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.map  -DXPRJ_default=$(CND_CONF)  -Wl,--defsym=__MPLAB_BUILD=1   -mdfp="${DFP_DIR}/xc8"  -fno-short-double -fno-short-float -memi=wordwrite -O0 -fasmfile -maddrqual=ignore -xassembler-with-cpp -mwarn=-3 -Wa,-a -msummary=-psect,-class,+mem,-hex,-file  -ginhx32 -Wl,--data-init -mno-keep-startup -mno-download -mdefault-config-bits -std=c99 -gdwarf-3 -mstack=compiled:auto:auto:auto     $(COMPARISON_BUILD) -Wl,--memorysummary,${DISTDIR}/memoryfile.xml -o ${DISTDIR}/Test_ST7735.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${OBJECTDIR}
	${RM} -r ${DISTDIR}

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
