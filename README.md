# Thank you for reading!
If the infromation from this project helped you in some way or another, please make a donation via [**Paypal**](https://paypal.me/BRV64?country.x=MX&locale.x=en_US). Hopefully in the future I can make a video explaining all this information. 

# Test_ST7735.X

This proyect has two main purposes
1. Show basic functionality of the small TFT ST7735 display on a PIC18F2550
2. Teach you on how to use VS Code as an alternative for MPLAB X

# ST7735 
TODO write documentation

[Reference Repo](https://github.com/sumotoy/TFT_ST7735/tree/1.0p1)

[ST7735 Datasheet](https://pdf1.alldatasheet.com/datasheet-pdf/view/326213/SITRONIX/ST7735.html)

# How to use Make file in VS Code

At the current time, there are 3 methods to use Make with MPLAB X

## Chocolatey

1. You can go to the official [Chocolatey](https://chocolatey.org/install) and proceed with the recommended installation (Too much text for my taste) or you can follow this [tutorial](https://jcutrer.com/windows/install-chocolatey-choco-windows10) to install it and move on with your life.
1. Install **Make** via PowerShell. You can also uninstall it if you wanna do that.
```
choco install make
```
3. Open your Project.X folder on VS code. You should find a Makefile that you can edit and modify as you like. By defaul you have 3 commands. There're more, but you figure it out.
> - Build a specific configuration
```
make build
```
> - Result. The percentages will change for sure as you edit Firmware.
```
Memory Summary:
    Program space        used    10h (    16) of  8000h bytes   (  0.0%)
    Data space           used     0h (     0) of   800h bytes   (  0.0%)
    Configuration bits   used     7h (     7) of     7h words   (100.0%)
    EEPROM space         used     0h (     0) of   100h bytes   (  0.0%)
    ID Location space    used     8h (     8) of     8h bytes   (100.0%)
```
> - Removes built files from a configuration
```
make clean
```
> - Removes all built files
```
make clobber
```
If this doesn't work, I wish you luck and continue to the next option

## GNUMAKE
If you have an error like thisone, you're into some wierd trip.
```
CreateProcess(NULL, gnumkdir 
```
Try the following!

1. Install [GNU Make](https://www.gnu.org/software/make/) and try running make in your favorite terminal. I guse CMD or PowerShell.

1. Repead Step 3 from Chocolatey. If this doesn't work, you'll have to use MPLAB X's make software.

## MPLABX's Make

Desperate means require desperate measures.

1. I uninstalled all make instances first. Not sure if necessary but I made sure to have a clean start.
1. You'll have to locate make on MPLAB X. v6.00 will change depending on current MPLABX version. It's located in your MPLAB X installation folder. Here you'll find **Make.exe**. It may look something like this: 
```
C:\Program Files\Microchip\MPLABX\v6.00\gnuBins\GnuWin32\bin
```
3. Follow this [tutorial](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/) to add enviroment variables.

1. In System Variables/Path you'll add a new path similar to point 2 from this section. 
1. You should have 3 windows open or something like that. Press **OK** in all of them.
1. **RESTART YOUR COMPUTER**. Don't miss this! It's no joke.

1. Open your project as specified in **Chocolatey Step 3**

# Additional commands
Added several commands to make your life easier (or harder). There are a few requirements before continuing.

1. Make sure you have [Java](https://www.java.com/en/download/manual.jsp) installed. If not, install it!

1. Make sure you've followed the **Make** instructions previously stated.

1. Read documentation located in your MPLABX installation folder. Specifically the [Readme](C:/Program%20Files/Microchip/MPLABX/v6.00/docs/Readme%20for%20IPECMD.htm) file. This file has all the documentation required to create commands.

## Download command

```
java -jar ${IPE_PATH}   -${TOOL_SELECTION_PT}${MP_PROGRAMMING_TOOL}  -${PART_SELECTION}${MP_PROCESSOR_OPTION}  -${HEX_FILE_SELECTION}${CND_ARTIFACT_PATH}  -${PROGRAM_DEVICE_REGION} -${RELEASE_FROM_RESET}
```
Lets break down this command.

- java -jar: Java Runtime Environment (JRE) directly calls IPE application
- IPE_PATH: IPE location. Normally at "MPLABX\ **YOUR_VERSION**\mplab_platform\mplab_ipe\ipecmd.jar"
- TOOL_SELECTION_PT: You can choose to identify you tool by Part(Tool name) "TP" or Serial Number "TS". This is written in the command exactly as "TP" since we're looking for a tool and don't have several tools connected.
    - P: Tool selection by short name
        - MPLAB REAL ICE    :RICE . Example: TPRICE
        - MPLAB ICE 4   :ICE4
        - MPLAB ICD 4   :ICD4
        - MPLAB ICD 3   :ICD3
        - MPLAB PICkit 4:PK4 . Example: TPPK4
        - MPLAB Snap    :SNAP
        - PICKIT 3  :PK3
        - MPLAB PM3 :PM3
        - PKOB  :PKOB
        - PKOB 4:PKOB4
        - EDBG  :EDBG
        - mEDBG :mEDBG
        - nEDBG :nEDBG
        - J-32  :J32
    - S: Tool selection by Serial Number. Example: "TSBUR123456789"
- MP_PROGRAMMING_TOOL: There are several tools that can be specified, PICKIT4 for example. This is written in the command exactly as "PK4"
- PART_SELECTION: Specify you're gonna select a part Number. This is written in the command exactly as "P".
- MP_PROCESSOR_OPTION: Your actual part number. After creating a project, you can find it in "Makefile-default.mk". This is written in the command exactly as "18F2550" in this case.
- HEX_FILE_SELECTION: You're gonna use a hex file. This is written in the command exactly as "F".
- CND_ARTIFACT_PATH: Hex file absolute or relative Path/location.
- PROGRAM_DEVICE_REGION: This is written in the command exactly as "M". In this case, no region is specified so the whole devide will be programmed. To program specific regions add one of the following letters to "M":
    - A = Auxiliary memory. Example "MA"
    - P = Program memory
    - E = EEPROM
    - I = ID memory
    - C = Configuration memory
    - B = Boot Flash Memory
- RELEASE_FROM_RESET: This command resets te microcontroller after programming. If this command is not added, you'll have to reset manually. Usefull when you're testing the internal EEPROM (I guess?). This is written in the command exactly as "OL".

### Resulting command

```
java -jar "C:\Program Files\Microchip\MPLABX\v6.00\mplab_platform\mplab_ipe\ipecmd.jar"   -TPPK4  -P18F2550  -F"C:\Users\YOUR_USER\MPLABXProjects\Test_ST7735.X\dist/default/production/YOUR_PROJECT_NAME.X.production.hex"  -M -OL
```

## Tools command
This one is far simpler. We only have one somewhat new command.
```
	java -jar ${IPE_PATH}   -T
```

- T: This commands specifies that you're gonna use a Tool. If no tool sellection is specified, it'll show current tools' information. Example:

```
Available Tool List
-------------------
1  PICkit 4 S.No : BUR123456789
```

## Download_sn command
Just as the **download** command. The only difference is the specified tool by serial number 

### Resulting command
```
java -jar "C:\Program Files\Microchip\MPLABX\v6.00\mplab_platform\mplab_ipe\ipecmd.jar"   -TSBUR123456789  -P18F2550  -F"C:\Users\YOUR_USER\MPLABXProjects\Test_ST7735.X\dist/default/production/YOUR_PROJECT_NAME.X.production.hex"  -M -OL
```
