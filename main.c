/*
 * File:   main.c
 * Author: brand
 *
 * Created on September 10, 2022, 11:40 PM
 */

#define __18F2550
#define _XTAL_FREQ 20000000

#include <xc.h>
#include "fuses.h"
#include "include/plib.h"

void main(void) {
    TRISBbits.TRISB4 = 0;
    
    int i;
    for (i = 0; i < 5; i++){
        LATBbits.LATB4 = ~LATBbits.LATB4;
        __delay_ms(100);
    }
    
    OpenSPI(SPI_FOSC_4, MODE_00, SMPMID);
    while(1){
        WriteSPI(0x14);
    }
    return;
}
