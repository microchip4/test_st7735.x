#ifndef _PLIB_H
#define _PLIB_H

#if	!defined(__ADC_H) && \
	!defined(__ANCOMP_H) && \
	!defined(__CAN2510_H) && \
	!defined(__CAPTURE_H) && \
	!defined(__COMPARE_H) && \
	!defined(__CTMU_H) && \
	!defined(__DPSLP_H) && \
	!defined(__EEP_H) && \
	!defined(__FLASH_H) && \
	!defined(__I2C_H) && \
	!defined(__IO_H) && \
	!defined(__MWIRE_H) && \
	!defined(__PCPWM_H) && \
	!defined(__PMP_H) && \
	!defined(__PORTB_H) && \
	!defined(__PPS_H) && \
	!defined(__PWM_H) && \
	!defined(__RESET_H) && \
	!defined(__RTCC_H) && \
	!defined(__SPI_H) && \
	!defined(__SW_I2C_H) && \
	!defined(__SW_SPI_H) && \
	!defined(__SW_UART_H) && \
	!defined(__TIMERS_H) && \
	!defined(__USART_H) && \
	!defined(__XLCD_H)

#include "plib/adc.h"
#include "plib/ancomp.h"
//#include "plib/can2510.h"
//#include "plib/can2510.h"
//#include "plib/compare.h"
#include "plib/ctmu.h"
#include "plib/dpslp.h"
#include "plib/EEP.h"
#include "plib/flash.h"
#include "plib/i2c.h"
#include "plib/io.h"
#include "plib/mwire.h"
#include "plib/pcpwm.h"
#include "plib/pmp.h"
#include "plib/portb.h"
#include "plib/pps.h"
//#include "plib/pwm.h"
//#include "plib/reset.h"
#include "plib/rtcc.h"
#include "plib/spi.h"
#include "plib/sw_i2c.h"
#include "plib/sw_spi.h"
#include "plib/sw_uart.h"
#include "plib/timers.h"
#include "plib/usart.h"
#include "plib/xlcd.h"
#include "plib/delays.h"

#endif

#endif
