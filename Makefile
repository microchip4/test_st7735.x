#
#  There exist several targets which are by default empty and which can be 
#  used for execution of your targets. These targets are usually executed 
#  before and after some main targets. They are: 
#
#     .build-pre:              called before 'build' target
#     .build-post:             called after 'build' target
#     .clean-pre:              called before 'clean' target
#     .clean-post:             called after 'clean' target
#     .clobber-pre:            called before 'clobber' target
#     .clobber-post:           called after 'clobber' target
#     .all-pre:                called before 'all' target
#     .all-post:               called after 'all' target
#     .help-pre:               called before 'help' target
#     .help-post:              called after 'help' target
#
#  Targets beginning with '.' are not intended to be called on their own.
#
#  Main targets can be executed directly, and they are:
#  
#     build                    build a specific configuration
#     clean                    remove built files from a configuration
#     clobber                  remove all built files
#     all                      build all configurations
#     help                     print help mesage
#  
#  Targets .build-impl, .clean-impl, .clobber-impl, .all-impl, and
#  .help-impl are implemented in nbproject/makefile-impl.mk.
#
#  Available make variables:
#
#     CND_BASEDIR                base directory for relative paths
#     CND_DISTDIR                default top distribution directory (build artifacts)
#     CND_BUILDDIR               default top build directory (object files, ...)
#     CONF                       name of current configuration
#     CND_ARTIFACT_DIR_${CONF}   directory of build artifact (current configuration)
#     CND_ARTIFACT_NAME_${CONF}  name of build artifact (current configuration)
#     CND_ARTIFACT_PATH_${CONF}  path to build artifact (current configuration)
#     CND_PACKAGE_DIR_${CONF}    directory of package (current configuration)
#     CND_PACKAGE_NAME_${CONF}   name of package (current configuration)
#     CND_PACKAGE_PATH_${CONF}   path to package (current configuration)
#
# NOCDDL


# Environment 
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
RANLIB=ranlib

MAKE_DIR=C:\'Program Files'\Microchip\MPLABX\v6.00\gnuBins\GnuWin32\bin\make.exe

TOOL_SN_1=BUR184652682

#Located in nbproject/Makefile-default.mk
MP_PROCESSOR_OPTION=18F2550
#Project Name (omiting ".X")
PROJECT_NAME=Test_ST7735

# build
build: .build-post

.build-pre:
# Add your pre 'build' code here...

.build-post: .build-impl
# Add your post 'build' code here...


# clean
clean: .clean-post

.clean-pre:
# Add your pre 'clean' code here...
# WARNING: the IDE does not call this target since it takes a long time to
# simply run make. Instead, the IDE removes the configuration directories
# under build and dist directly without calling make.
# This target is left here so people can do a clean when running a clean
# outside the IDE.

.clean-post: .clean-impl
# Add your post 'clean' code here...


# clobber
clobber: .clobber-post

.clobber-pre:
# Add your pre 'clobber' code here...

.clobber-post: .clobber-impl
# Add your post 'clobber' code here...


# all
all: .all-post

.all-pre:
# Add your pre 'all' code here...

.all-post: .all-impl
# Add your post 'all' code here...


# help
help: .help-post
	@echo ------- Commands --------
	@echo build		: Builds curren project
	@echo -------------------
	@echo clean		: Removes built files from a configuration
	@echo -------------------
	@echo clobber		: Removes all built files
	@echo -------------------
	@echo download	: Programs current HW. You need to update Make with new Folder configurations
	@echo -------------------
	@echo download_sn	: Programs current HW with specific SN. You need to update Make with new Folder configurations
	@echo -------------------
	@echo tools		: Shows Serial number of current connected tools
	@echo -------------------
.help-pre:
# Add your pre 'help' code here...

.help-post:
# Add your post 'help' code here...

#Command Line Options. More info at file:///C:/Program%20Files/Microchip/MPLABX/v6.00/docs/Readme%20for%20IPECMD.htm
#Help
HELP_SCREEN=?
#Select a voltage Examp: A3.3
VDDAPP=A

ENVIRONMENT_OPERATION=B
#Locate the hex file you want to use
HEX_FILE_SELECTION=F
#Program the entire device. You can specify memory locations. More info at... you know.
PROGRAM_DEVICE_REGION=M
#Resets device after programming. Really helpfull!
RELEASE_FROM_RESET=OL
#Select a part Ex: PIC16F887 should be 16F887
PART_SELECTION=P
#Select tool by Part
TOOL_SELECTION_PT=TP
#Select tool by Serial Number
TOOL_SELECTION_SN=TS

#Choose your tool. More info at file:///C:/Program%20Files/Microchip/MPLABX/v6.00/docs/Readme%20for%20IPECMD.htm
MPLAB_PM3=PM3
MPLAB_SNAP=SNAP
MPLAB_PICKIT3=PK3
MPLAB_PICKIT4=PK4
MPLAB_REAL_ICE=RICE
ATMEL_ICE=AICE
MPLAB_ICE4=ICE4
MPLAB_ICD4=ICD4
MPLAB_ICD3=ICD3
PROGRAMMER_KIT_ON_BOARD=PKOB
PROGRAMMER_KIT_ON_BOARD4=PKOB4
ATMEL_EMBEDDED_DEBUGGER=EDBG
ATMEL_MINI_EMBEDDED_DEBUGGER=MEDBG
ATMEL_NANO_EMBEDDED_DEBUGGER=NEDBG
J32_DEBUG=J32 
#Selected tool
MP_PROGRAMMING_TOOL=${MPLAB_PICKIT4}

#Programming tool file location
IPE_PATH="C:\Program Files\Microchip\MPLABX\v6.00\mplab_platform\mplab_ipe\ipecmd.jar"
#Relative hex file path
CND_ARTIFACT_PATH_default=dist\default\production\${PROJECT_NAME}.X.production.hex
#Unique path Relative
CND_ARTIFACT_PATH="$(shell pwd)\${CND_ARTIFACT_PATH_default}"

#include nbproject/Makefile-default.mk
# program
download: 
#.program-pre
	java -jar ${IPE_PATH}   -${TOOL_SELECTION_PT}${MP_PROGRAMMING_TOOL}  -${PART_SELECTION}${MP_PROCESSOR_OPTION}  -${HEX_FILE_SELECTION}${CND_ARTIFACT_PATH}  -${PROGRAM_DEVICE_REGION} -${RELEASE_FROM_RESET}

download_sn:
	java -jar ${IPE_PATH}   -${TOOL_SELECTION_SN}${TOOL_SN_1}  -${PART_SELECTION}${MP_PROCESSOR_OPTION}  -${HEX_FILE_SELECTION}${CND_ARTIFACT_PATH}  -${PROGRAM_DEVICE_REGION} -${RELEASE_FROM_RESET}

tools:
	java -jar ${IPE_PATH}   -T

# include project implementation makefile
include nbproject/Makefile-impl.mk

# include project make variables
include nbproject/Makefile-variables.mk
