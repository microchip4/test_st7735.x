
#ifndef __SWI2C16D_H
#define __SWI2C16D_H

#include "../../../../include/plib/sw_i2c.h"


/*****   VARIABLES DECLARED FOR PIC18CXXX   *****/

extern unsigned char I2C_BUFFER;    // temp buffer for R/W operations
extern unsigned char BIT_COUNTER;   // temp buffer for bit counting

/*  end  swi2c16d.h file */

#endif

